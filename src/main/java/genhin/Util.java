package genhin;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import genhin.json.*;
import genhin.math.AttackValue;
import genhin.math.Characteristic;
import genhin.math.StatisticCrit;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public class Util {   

    private final static ObjectMapper mapper = JsonMapper.builder().build();


    public static DpsMetrConteiner reader(File fileCharList) {
        if (fileCharList.exists()) {
            try {
                //JSON file to Java object
                return mapper.readValue(fileCharList, DpsMetrConteiner.class);
            } catch (IOException e) {
                log.debug(   e.toString()  );
            }
        } else {
            log.debug("Нет файла:" + fileCharList.getAbsolutePath());
        }
        return null;
    }

    public static void write(DpsMetrConteiner dpsMetrConteiner, File fileCharList) {
        try {
            // Java object to JSON file
            mapper.writeValue(fileCharList, dpsMetrConteiner);
        } catch (IOException e) {
            log.debug(   e.toString()  );
        }
    }


    public static Hero getSelectHero(GroupHero groupHero) {

        Optional< Hero> heroe = groupHero.getHeroes()
                .stream()
                .filter(f -> f.isSelect())
                .findFirst();
        return heroe.get();
    }

    public static List< Hero> getOtherHero(GroupHero groupHero) {
        return groupHero.getHeroes()
                .stream()
                .filter(f -> !f.isSelect())
                .collect(Collectors.toList());
    }

    public static List<ValueOption> getSelectHeroValueOption(GroupHero groupHero) {
        return groupHero.getHeroes()
                .stream()
                .filter(f -> f.isSelect())
                .flatMap(m -> m.getOptions().stream())
                .collect(Collectors.toList());
    }

    public static List<ValueOption> getHerosAffectsGroup(GroupHero groupHero) {
        return groupHero.getHeroes()
                .stream()
                .filter(
                        f -> !f.isSelect() &&
                                f.getOptions().stream().filter(gh -> gh.isAffectsGroup()).count() > 0
                ).flatMap(m -> m.getOptions().stream())
                .collect(Collectors.toList());
    }

    /**
     * Шаблон РАсчёт основных параметров {@link TypeOption#HP}, {@link TypeOption#ATK}, {@link TypeOption#DEF}
     *
     * @param groupHero
     * @param typeOption
     * @param percent
     * @param units
     * @return
     */
    public static double calcValueTypeOption(GroupHero groupHero, TypeOption typeOption, TypeOption percent, TypeOption units) {
        Double value = 0.0d;
        Double valuePercent = 0.0d;
        Double valuePercentArtifact = 0.0d;

        List<ValueOption> allOptions = new ArrayList<>();
        allOptions.addAll(Util.getSelectHeroValueOption(groupHero));
        allOptions.addAll(Util.getHerosAffectsGroup(groupHero));

        //Персонаж/Оружие статы
        value = allOptions.stream()
                .filter(f -> (TypeGroup.CHARACTER.equals(f.getTypeGroup()) || TypeGroup.WEAPON.equals(f.getTypeGroup())) && typeOption.equals(f.getTypeOption()))
                .mapToDouble(d -> d.getValueMain())
                .sum();

        //Артефакты проценты
        valuePercent = allOptions.stream()
                .filter(f -> !TypeGroup.TALENT.equals(f.getTypeGroup()) &&
                        percent.equals(f.getTypeOption()) &&
                        !TypeAbility.NOT.equals(f.getTypeAbility()))
                .mapToDouble(d -> d.getValueMain())
                .sum();

        //Артефакты единицы
        valuePercentArtifact = allOptions.stream()
                .filter(f -> (TypeGroup.ARTIFACT.equals(f.getTypeGroup()) || TypeGroup.FOOD.equals(f.getTypeGroup())) && units.equals(f.getTypeOption()))
                .mapToDouble(d -> d.getValueMain())
                .sum();

        return value * (1.0d + valuePercent / 100.0d) + valuePercentArtifact;
    }


    public static double coefficientEm(GroupHero groupHero) {
        Double elm = 0.0d;

        List<ValueOption> allOptions = new ArrayList<>();
        allOptions.addAll(Util.getSelectHeroValueOption(groupHero));
        allOptions.addAll(Util.getHerosAffectsGroup(groupHero));

        //Персонаж/Оружие статы
        elm = allOptions.stream()
                .filter(f ->
                        (TypeGroup.CHARACTER.equals(f.getTypeGroup()) || TypeGroup.WEAPON.equals(f.getTypeGroup()) || TypeGroup.ARTIFACT.equals(f.getTypeGroup()) ||
                                TypeGroup.CONSTELLATION.equals(f.getTypeGroup()) || TypeGroup.TALENT.equals(f.getTypeGroup())) && TypeOption.ELM.equals(f.getTypeOption())
                )
                .mapToDouble(d -> d.getValueMain())
                .sum();


        return elm;
    }

    public static double coefficientEm1(double elm) {
        return Math.ceil(25.0d / 9.0d * elm / (1401.0d + elm) * 100.0d * 10.0d) / 10.0d;
    }

    public static double coefficientEm2(double elm) {
        return Math.ceil(60.0d / 9.0d * elm / (1401.0d + elm) * 100.0d * 10.0d) / 10.0d;
    }

    public static double coefficientEm3(double elm) {
        return Math.ceil(40.0d / 9.0d * elm / (1401.0d + elm) * 100.0d * 10.0d) / 10.0d;
    }


    public static double coefficientCritChance(GroupHero groupHero) {
        Double critRate = 0.0d;
        List<ValueOption> allOptions = new ArrayList<>();
        allOptions.addAll(Util.getSelectHeroValueOption(groupHero));
        allOptions.addAll(Util.getHerosAffectsGroup(groupHero));


        critRate = allOptions.stream()
                .filter(f ->
                        // (TypeGroup.CHARACTER.equals(o.getTypeGroup()) ||TypeGroup.WEAPON.equals(o.getTypeGroup()) ||TypeGroup.ARTIFACT.equals(o.getTypeGroup()) || TypeGroup.CONSTELLATION.equals(o.getTypeGroup()) || TypeGroup.TALENT.equals(o.getTypeGroup())) &&
                        TypeOption.CRIT_CHANCE_PERCENT.equals(f.getTypeOption())
                )
                .mapToDouble(d -> d.getValueMain())
                .sum();

        return critRate;
    }

    /**
     * @param groupHero
     * @return
     */
    public static double coefficientCritDmg(GroupHero groupHero) {
        Double critDmg = 0.0d;
        List<ValueOption> allOptions = new ArrayList<>();
        allOptions.addAll(Util.getSelectHeroValueOption(groupHero));
        allOptions.addAll(Util.getHerosAffectsGroup(groupHero));

        critDmg = allOptions.stream()
                .filter(f ->
                        (TypeGroup.CHARACTER.equals(f.getTypeGroup()) || TypeGroup.WEAPON.equals(f.getTypeGroup()) || TypeGroup.ARTIFACT.equals(f.getTypeGroup()) ||
                                TypeGroup.CONSTELLATION.equals(f.getTypeGroup()) || TypeGroup.TALENT.equals(f.getTypeGroup())) &&
                                (
                                        TypeOption.CRIT_PERCENT.equals(f.getTypeOption())
                                )
                )
                .mapToDouble(d -> d.getValueMain())
                .sum();

        return critDmg;
    }

    /**
     * Вичисление процентного бонуса элементального урона
     *
     * @param groupHero
     * @param typeOption
     * @return
     */
    public static double calcElement(GroupHero groupHero, TypeOption typeOption) {
        Double element = 0.0d;
        List<ValueOption> allOptions = new ArrayList<>();
        allOptions.addAll(Util.getSelectHeroValueOption(groupHero));
        allOptions.addAll(Util.getHerosAffectsGroup(groupHero));


        element = allOptions.stream()
                .filter(f -> typeOption.equals(f.getTypeOption()))
                .mapToDouble(d -> d.getValueMain())
                .sum();

        return element;
    }


    /**
     * Вичисление процентного бонуса элементального урона
     *
     * @param groupHero
     * @param typeOption
     * @return
     */
    public static double calcReactionBonuses(GroupHero groupHero, TypeElementStatus typeOption) {
        Double element = 0.0d;
        List<ValueOption> allOptions = new ArrayList<>();
        allOptions.addAll(Util.getSelectHeroValueOption(groupHero));
        allOptions.addAll(Util.getHerosAffectsGroup(groupHero));


        element = allOptions.stream()
                .filter(f ->
                        TypeGroup.ARTIFACT.equals(f.getTypeGroup()) &&
                                typeOption.equals(f.getTypeElementStatus())
                )
                .mapToDouble(d -> d.getValueMain())
                .sum();

        return element;
    }


    /**
     * @param groupHero
     * @param typeDmgBonus
     * @return
     */
    public static double coefficientDmgBonus(GroupHero groupHero, TypeOption typeDmgBonus) {
        Double dmgBonus = 0.0d;
        List<ValueOption> allOptions = new ArrayList<>();
        allOptions.addAll(Util.getSelectHeroValueOption(groupHero));
        allOptions.addAll(Util.getHerosAffectsGroup(groupHero));

        dmgBonus = allOptions.stream()
                .filter(f ->
                        (
                                TypeGroup.ARTIFACT.equals(f.getTypeGroup()) ||
                                        TypeGroup.POTION.equals(f.getTypeGroup()) ||
                                        TypeGroup.FOOD.equals(f.getTypeGroup())
                        ) &&
                                typeDmgBonus.equals(f.getTypeOption())
                )
                .mapToDouble(d -> d.getValueMain())
                .sum();

        return (1.0d + dmgBonus / 100.0d);
    }

    /**
     * @param groupHero
     * @param enemies
     * @param damageTypeElement
     * @return
     */
    public static double coefficientRes(GroupHero groupHero, Enemies enemies, TypeElement damageTypeElement) {
        Double res = 0.0d;
        Double resEnemies = 0.0d;
        List<ValueOption> allOptions = new ArrayList<>();
        allOptions.addAll(Util.getSelectHeroValueOption(groupHero));
        allOptions.addAll(Util.getHerosAffectsGroup(groupHero));

        res = allOptions.stream()
                .filter(f -> TypeOption.RES_PERCENT.equals(f.getTypeOption()) && damageTypeElement.equals(f.getDamageTypeElement()))
                .mapToDouble(d -> d.getValueMain())
                .sum();


        resEnemies = enemies.getOptions().stream()
                .filter(f -> TypeOption.RES_PERCENT.equals(f.getTypeOption()) && damageTypeElement.equals(f.getDamageTypeElement()))
                .mapToDouble(d -> d.getValueMain())
                .sum();

        return calcRes(res, resEnemies);
    }


    /**
     * Resistance - Сопротивление урону в процентах
     *
     * @param resPersent        Влияние на Сопротивление Параметры из:
     *                          {@link TypeOption#RES_PERCENT},
     *                          {@link TypeElement}
     * @param resEnemiesPersent Сопративление врага
     * @return коэффициент 1.05   0.24
     */
    public static double calcRes(double resPersent, double resEnemiesPersent) {
        double resD = resEnemiesPersent / 100.0d + resPersent / 100.0d;

        if (resD < 0.0d) {
            return 1.0d - (resD / 2.0d);
        }

        if (0.0d <= resD && resD < 0.75f) {
            return 1.0d - resD;
        }

        if (resD >= 0.75f) {
            return 1 / (4.0d * resD + 1.0d);
        }
        return resD;
    }

    /**
     * Defense - Защита от атаки
     * *(1- ((LvL_Atak+100)/((1-DEFreduction) * (LvL_Atak+100)+(LvL_HERO+100))) )
     *
     * @param groupHero
     * @param enemies
     * @return
     */
    public static double coefficientDef(GroupHero groupHero, Enemies enemies) {
        Double defReduction = 0.0d;
        List<ValueOption> allOptions = new ArrayList<>();
        allOptions.addAll(Util.getSelectHeroValueOption(groupHero));
        allOptions.addAll(Util.getHerosAffectsGroup(groupHero));

        defReduction = allOptions.stream()
                .filter(f -> TypeOption.DEF_PERCENT.equals(f.getTypeOption()))
                .mapToDouble(d -> d.getValueMain())
                .sum() / 100.0d;

        return calcDamageReduction(Util.getSelectHero(groupHero).getLvl(), enemies.getLvl(), defReduction);
    }

    /**
     * Понижение урона от уровня и преодоления защиты
     *
     * @param lvlHero      {@link Hero#getLvl()}
     * @param lvlEnemy     {@link Enemies#getLvl()}
     * @param defReduction Определяется значениями по группе  {@link ValueOption#getIndex()}
     */
    public static double calcDamageReduction(double lvlHero, double lvlEnemy, double defReduction) {
        return (1.0d - (
                (lvlEnemy + 100.0d)
                        /
                        ((1 - defReduction) * (lvlEnemy + 100.0d) + (lvlHero + 100.0d))
        ));
    }


    /**
     * Reaction - Расчёт урона от реакции
     * (Reaction_Multiplier  * (1 + EM%  + Other_Reaction_Bonuses ))
     *
     * @param enemies
     * @param characteristic
     * @param attackValue    Тип нанесённого урона
     * @return
     */
    public static Double coefficientReaction(Enemies enemies, Characteristic characteristic, AttackValue attackValue) {
        //Поиск Элемента на мобе
        ValueOption elementEnemies = enemies.getOptions().stream()
                .filter(f -> TypeGroup.ELEMENT.equals(f.getTypeGroup()))
                .findFirst().orElse(new ValueOption());

        definitionReactionMultiplier(elementEnemies.getDamageTypeElement(), attackValue);
        Double reactionBonuses = 0.0d;
        Double elementalMastery = 0.0d;

        if (attackValue.getTypeElementStatus() != null) {
            switch (attackValue.getTypeElementStatus()) {
                case VAPORIZE:
                    reactionBonuses = characteristic.getVaporize();
                    elementalMastery = characteristic.getElm1();
                    break;
                case BURNING:
                    reactionBonuses = characteristic.getBurning();
                    break;
                case OVERLOADED:
                    reactionBonuses = characteristic.getOverloaded();
                    elementalMastery = characteristic.getElm2();
                    break;
                case MELT:
                    reactionBonuses = characteristic.getMelt();
                    elementalMastery = characteristic.getElm1();
                    break;
                case ELECTRO_CHARGED:
                    reactionBonuses = characteristic.getElectroCharged();
                    elementalMastery = characteristic.getElm2();
                    break;
                case FROZEN:
                    reactionBonuses = characteristic.getFrozen();
                    elementalMastery = characteristic.getElm2();
                    break;
                case SUPERCONDUCT:
                    reactionBonuses = characteristic.getSuperconduct();
                    elementalMastery = characteristic.getElm2();
                    break;
                case SWIRL:
                    reactionBonuses = characteristic.getSwirl();
                    elementalMastery = characteristic.getElm2();
                    break;
                case CRYSTALLIZE:
                    reactionBonuses = characteristic.getCrystallize();
                    elementalMastery = characteristic.getElm3();
                    break;
            }

            return attackValue.getReactionMultiplier() * (1.0d + (reactionBonuses + elementalMastery) / 100.0d);
        } else {//Нет реакции
            return attackValue.getReactionMultiplier();
        }
    }

    private static void definitionReactionMultiplier(TypeElement elementEnemies, AttackValue attackValue) {

        //Статус элемента и множитель
        switch (attackValue.getDamageTypeElement()) {
            case FIRE:
                if (TypeElement.HYDRO.equals(attackValue)) {
                    attackValue.setReactionMultiplier(1.5d);
                    attackValue.setTypeElementStatus(TypeElementStatus.VAPORIZE);
                }
                if (TypeElement.DENDRO.equals(attackValue)) {
                    attackValue.setTypeElementStatus(TypeElementStatus.BURNING);
                }
                if (TypeElement.ELECTRO.equals(attackValue)) {
                    attackValue.setTypeElementStatus(TypeElementStatus.OVERLOADED);
                }
                if (TypeElement.CRYO.equals(attackValue)) {
                    attackValue.setReactionMultiplier(2.0d);
                    attackValue.setTypeElementStatus(TypeElementStatus.MELT);
                }
                break;
            case HYDRO:
                if (TypeElement.FIRE.equals(attackValue)) {
                    attackValue.setReactionMultiplier(2.0d);
                    attackValue.setTypeElementStatus(TypeElementStatus.VAPORIZE);
                }
                if (TypeElement.ELECTRO.equals(attackValue)) {
                    attackValue.setTypeElementStatus(TypeElementStatus.ELECTRO_CHARGED);
                    //todo создает разряд молнии какой урон ?
                }
                if (TypeElement.CRYO.equals(attackValue)) {
                    attackValue.setTypeElementStatus(TypeElementStatus.FROZEN);
                    //todo доп урон какое значение ?
                }
                break;
            case DENDRO:
                if (TypeElement.FIRE.equals(attackValue)) {
                    attackValue.setTypeElementStatus(TypeElementStatus.BURNING);
                }
                break;
            case ELECTRO:
                if (TypeElement.FIRE.equals(attackValue)) {
                    attackValue.setTypeElementStatus(TypeElementStatus.OVERLOADED);
                }
                if (TypeElement.HYDRO.equals(attackValue)) {
                    attackValue.setTypeElementStatus(TypeElementStatus.ELECTRO_CHARGED);
                    //todo создает разряд молнии какой урон ?
                }
                if (TypeElement.CRYO.equals(attackValue)) {
                    attackValue.setTypeElementStatus(TypeElementStatus.SUPERCONDUCT);
                    //цели физическому урону на -40% на 12 секунд.
                    attackValue.setValueOption(new ValueOption(TypeGroup.ELEMENT, TypeOption.PHYS_PERCENT, 40.0f));
                }
                break;
            case ANEMO:
                attackValue.setTypeElementStatus(TypeElementStatus.SWIRL);
                break;
            case CRYO:
                if (TypeElement.FIRE.equals(attackValue)) {
                    attackValue.setReactionMultiplier(1.5d);
                    attackValue.setTypeElementStatus(TypeElementStatus.MELT);
                }
                if (TypeElement.HYDRO.equals(attackValue)) {
                    attackValue.setTypeElementStatus(TypeElementStatus.FROZEN);
                }
                if (TypeElement.ELECTRO.equals(attackValue)) {
                    attackValue.setTypeElementStatus(TypeElementStatus.SUPERCONDUCT);
                    //цели физическому урону на -40% на 12 секунд.
                    attackValue.setValueOption(new ValueOption(TypeGroup.ELEMENT, TypeOption.PHYS_PERCENT, 40.0f));
                }
                break;
            case GEO:
                break;
            case PHYS:
                break;
            default:
                throw new RuntimeException("Не заданэлемент атаки");
        }


    }

    /**
     * Статистика крит урона + среднее значение
     * @param characteristic
     * @return
     */
    public static StatisticCrit coefficientCrit(Characteristic characteristic) {
        int count = 100000;
        StatisticCrit statisticCrit = new StatisticCrit();
        statisticCrit.setAttakCount(count);
        statisticCrit.setCritDmg(characteristic.getCritDmg());
        statisticCrit.setCritChance(characteristic.getCritChance());
        //Шанс крита
        DoubleSummaryStatistics randomCrit = new Random().ints(0, 10000)
                .limit(count)
                .mapToDouble(s -> s)
                .map(m -> {
                    //Если удар удазный записываем
                    double critRate = m / 100.0d;
                    if (characteristic.getCritChance() < critRate) {
                        statisticCrit.critSumOn();
                    }
                    return m;
                }).summaryStatistics();
        statisticCrit.setSummaryStatistics(randomCrit);

        return statisticCrit;
    }


    //////////////////////////////////

    /**
     * Расчёт урона от Таланта, начало всех расчётов
     *
     * @param characteristic
     * @param groupHero
     * @param combo
     * @return
     */
    public static List<AttackValue> valueTalentAttack(Characteristic characteristic, GroupHero groupHero, int combo, TypeAbility ability) {
        List<AttackValue> attackValues = new ArrayList<>();

        List<ValueOption> allOptions = new ArrayList<>();
        allOptions.addAll(Util.getSelectHeroValueOption(groupHero));
        allOptions.addAll(Util.getHerosAffectsGroup(groupHero));

        List<ValueOption> attackAbility = allOptions.stream()
                .filter(f ->
                        ability.equals(f.getTypeAbility()) &&
                                combo == f.getIndex()
                ).collect(Collectors.toList());

        attackAbility.forEach(f -> {
            double mainValue = 0.0d;
            switch (f.getTypeOption()) {
                case HP:
                    mainValue = characteristic.getHp();
                    break;
                case ATK:
                    mainValue = characteristic.getAtk();
                    break;
                case DEF:
                    mainValue = characteristic.getDef();
                    break;
            }


            //TypeChar.ATK * valueMult
            //TypeChar.ATK * valueMult * teak
            attackValues.add(new AttackValue(f.getName(), mainValue * f.getValueMain() * f.getTeak(), f.getTimeAniMs(), 1, f.getDamageTypeElement()));
            //ТОлько вторая часть
            //TypeChar.ATK * valueMult + TypeChar.ATK * valueMult2
            if (f.getValueMult2() != 0.0d) {
                attackValues.add(new AttackValue(f.getName() + "_Mult2", mainValue * f.getValueMult2(), f.getTimeAniMs(), 2, f.getDamageTypeElement()));
            }
        });


        return attackValues;
    }


}
