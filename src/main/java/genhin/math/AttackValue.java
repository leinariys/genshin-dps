package genhin.math;

import genhin.json.TypeElement;
import genhin.json.TypeElementStatus;
import genhin.json.ValueOption;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@AllArgsConstructor
@Data
public class AttackValue {

    private String name;
    private Double atk;
    private Double time;
    //Для последовательности урона таких как valueMult2 чтобы реакция
    private int index;
    private TypeElement damageTypeElement;


    private Double coefficientDmgBonus;
    private Double coefficientRes;
    private Double coefficientDef;
    private Double coefficientReaction;
    private StatisticCrit coefficientCrit;

    private Double reactionMultiplier = 1.0d;
    private TypeElementStatus typeElementStatus;

    //Предполагаемых следующих эффектов
    private List<ValueOption> options = new ArrayList<ValueOption>();

    public AttackValue() {
    }

    public AttackValue(String name, Double atk, Double time, int index, TypeElement damageTypeElement) {
        this.name = name;
        this.atk = atk;
        this.time = time;
        this.index = index;
        this.damageTypeElement = damageTypeElement;
    }

    public void setValueOption(ValueOption... valueOption) {
        options.addAll(Arrays.asList(valueOption));
    }

}
