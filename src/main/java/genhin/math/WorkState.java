package genhin.math;

import genhin.Util;
import genhin.json.*;
import lombok.Getter;

import java.util.List;


/**
 * Расчёт состояния пати героев
 */
public class WorkState {

    private GroupHero groupHero;
    @Getter
    private Characteristic characteristic;
    private Enemies enemies;


    public WorkState(GroupHero groupHero, Enemies enemies) {
        this.groupHero = groupHero;
        this.characteristic = new Characteristic();
        this.enemies = enemies;
        updateState();
    }

    /**
     * Перерасчёт статов
     */
    private void updateState() {
        characteristic.updateState(groupHero);
    }

    /**
     * Перерасчёт коэффициеннтов атаки
     *
     * @param attackValues
     */
    private void setCoefficient(List<AttackValue> attackValues) {
        for (AttackValue item : attackValues) {
            //Статы DmgBonus
            item.setCoefficientDmgBonus(Util.coefficientDmgBonus(groupHero, TypeOption.getType(item.getDamageTypeElement() + "_PERCENT")));
            //Статы Res
            item.setCoefficientRes(Util.coefficientRes(groupHero, enemies, item.getDamageTypeElement()));
            //Статы Def
            item.setCoefficientDef(Util.coefficientDef(groupHero, enemies));
            //Статы Reaction
            item.setCoefficientReaction(Util.coefficientReaction(enemies, characteristic, item));
            //Статы Crit
            item.setCoefficientCrit(Util.coefficientCrit(characteristic));


        }
    }


    /**
     * Атака обычная Ability
     * <p>
     * runTalentAttack
     * combo:
     * TypeChar.ATK * valueMult
     * TypeChar.ATK * valueMult * teak
     * TypeChar.ATK * valueMult + TypeChar.ATK * valueMult2
     *
     * @param combo
     * @return
     */
    public List<AttackValue> runTalentAttack(int combo) {
        updateState();
        List<AttackValue> attackValues = Util.valueTalentAttack(characteristic, groupHero, combo, TypeAbility.ATTACK);
        setCoefficient(attackValues);
        return attackValues;
    }


    /**
     * Атака прицельный
     * <p>
     * runTalentAttackAim
     * combo:
     * TypeChar.ATK * valueMult
     *
     * @return
     */
    public List<AttackValue> runTalentAttackAim(int combo) {
        updateState();
        List<AttackValue> attackValues = Util.valueTalentAttack(characteristic, groupHero, combo, TypeAbility.ATTACK_AIM);
        setCoefficient(attackValues);
        return attackValues;
    }

    /**
     * Атака заряженная
     * runTalentAttackCharged
     * TypeChar.ATK * valueMult
     * TypeChar.ATK * valueMult + TypeChar.ATK * valueMult2
     *
     * @return
     */
    public List<AttackValue> runTalentAttackCharged() {
        updateState();
        List<AttackValue> attackValues = Util.valueTalentAttack(characteristic, groupHero, 0, TypeAbility.ATTACK_CHARGED);
        setCoefficient(attackValues);
        return attackValues;
    }

    /**
     * Атака в падении
     * runTalentAttackFall
     * TypeChar.ATK * valueMult
     *
     * @return
     */
    public List<AttackValue> runTalentAttackFall() {
        updateState();
        List<AttackValue> attackValues = Util.valueTalentAttack(characteristic, groupHero, 0, TypeAbility.ATTACK_FALL);
        setCoefficient(attackValues);
        return attackValues;
    }

    /**
     * Атака Низко/высоко
     * runTalentAttackUpDown
     * TypeChar.ATK * valueMult  + TypeChar.ATK * valueMult2
     *
     * @return
     */
    public List<AttackValue> runTalentAttackUpDown() {
        updateState();
        List<AttackValue> attackValues = Util.valueTalentAttack(characteristic, groupHero, 0, TypeAbility.ATTACK_UP);
        setCoefficient(attackValues);
        return attackValues;
    }

    /**
     * Элементальный навык
     * runTalentE
     * combo:
     * TypeChar.ATK * valueMult
     * TypeChar.HP * valueMult + valueAdd
     * TypeChar.ELM * valueMult * teak
     * TypeChar.ATK * valueMult + TypeChar.ATK * valueMult2
     *
     * @param combo
     * @return
     */
    public List<AttackValue> runTalentE(int combo) {
        updateState();
        List<AttackValue> attackValues = Util.valueTalentAttack(characteristic, groupHero, combo, TypeAbility.E);
        setCoefficient(attackValues);
        return attackValues;
    }

    /**
     * Взрыв стихий
     * runTalentQ
     * combo:
     * TypeChar.ATK * valueMult
     * TypeChar.ATK * valueMult + valueAdd
     * TypeChar.ELM * valueMult * teak //Сахароза
     * TypeChar.runTalentAttack * valueMult // рейзор
     *
     * @return
     */
    public List<AttackValue> runTalentQ(int combo) {
        updateState();
        List<AttackValue> attackValues = Util.valueTalentAttack(characteristic, groupHero, combo, TypeAbility.Q);
        setCoefficient(attackValues);
        return attackValues;
    }


    public String descript() {
        return descript(false, false);
    }

    public String descript(boolean isDmgBonus, boolean isElementBonus) {
        StringBuilder stat = new StringBuilder();
        Hero select = Util.getSelectHero(groupHero);

        stat.append("Имя: " + select.getName()).append('\n');
        stat.append("Уровень: " + select.getLvl()).append('\n');
        stat.append("Статы").append('\n');

        stat.append("Здоровье: " + characteristic.getHp()).append('\n');
        stat.append("Атака: " + characteristic.getAtk()).append('\n');
        stat.append("Защита: " + characteristic.getDef()).append('\n');

        stat.append("Мастерство: " + characteristic.getElm()).append('\n');
        stat.append("Мастерство 1: " + characteristic.getElm1()).append('\n');
        stat.append("Мастерство 2: " + characteristic.getElm2()).append('\n');
        stat.append("Мастерство 3: " + characteristic.getElm3()).append('\n');

        stat.append("Крит Шанс: " + characteristic.getCritChance()).append('\n');
        stat.append("Крит: " + characteristic.getCritDmg()).append('\n');

        if (isDmgBonus) {
            stat.append("Healing: " + characteristic.getHealingPercent()).append('\n');
            stat.append("Energy: " + characteristic.getEnergyPercent()).append('\n');
            stat.append("Fire: " + characteristic.getFirePercent()).append('\n');
            stat.append("Hydro: " + characteristic.getHydroPercent()).append('\n');
            stat.append("Dendro: " + characteristic.getDendroPercent()).append('\n');
            stat.append("Electro: " + characteristic.getElectroPercent()).append('\n');
            stat.append("Anemo: " + characteristic.getAnemoPercent()).append('\n');
            stat.append("Cryo: " + characteristic.getCryoPercent()).append('\n');
            stat.append("Geo: " + characteristic.getGeoPercent()).append('\n');
            stat.append("Phys: " + characteristic.getPhysPercent()).append('\n');
        }
        if (isElementBonus) {
            stat.append("Vaporize: " + characteristic.getVaporize()).append('\n');
            stat.append("Burning: " + characteristic.getBurning()).append('\n');
            stat.append("Overloaded: " + characteristic.getOverloaded()).append('\n');
            stat.append("Melt: " + characteristic.getMelt()).append('\n');
            stat.append("ElectroCharged: " + characteristic.getElectroCharged()).append('\n');
            stat.append("Frozen: " + characteristic.getFrozen()).append('\n');
            stat.append("Superconduct: " + characteristic.getSuperconduct()).append('\n');
            stat.append("Swirl: " + characteristic.getSwirl()).append('\n');
            stat.append("Crystallize: " + characteristic.getCrystallize()).append('\n');
        }

        stat.append(String.format("Атака:"));
        runTalentAttack(1).stream().forEach(i -> {
            stat.append(String.format(" %s", i.getName()));
        });
        stat.append('\n');

        stat.append(String.format("Итого Дпс: 291,3")).append('\n');
        stat.append(String.format("Время Анимации: 2s500mm")).append('\n');
        stat.append(String.format("ДПС: 116.52 е/сек")).append('\n').append('\n');


        stat.append(String.format("Противник Имя: %s", enemies.getName())).append('\n');
        stat.append(String.format("Противник lvl: %s", enemies.getLvl())).append('\n');
        stat.append(String.format("Противник резист: %s", enemies.getRes(TypeElement.ELECTRO))).append('\n');

        return stat.toString();
    }


}
