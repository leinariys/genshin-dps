package genhin.math;

import genhin.Util;
import genhin.json.GroupHero;
import genhin.json.TypeElementStatus;
import genhin.json.TypeOption;
import lombok.Data;

@Data
public class Characteristic {


    private double hp;
    private double atk;
    private double def;
    private double elm;
    private double elm1;
    private double elm2;
    private double elm3;
    private double critChance;
    private double critDmg;
    private double healingPercent;
    private double energyPercent;

    private double firePercent;
    private double hydroPercent;
    private double dendroPercent;
    private double electroPercent;
    private double anemoPercent;
    private double cryoPercent;
    private double geoPercent;
    private double physPercent;


    //Реакции Other_Reaction_Bonuses
    private double vaporize;
    private double burning;
    private double overloaded;
    private double melt;
    private double electroCharged;
    private double frozen;
    private double superconduct;
    private double swirl;
    private double crystallize;


    public void updateState(GroupHero groupHero) {
        hp = Util.calcValueTypeOption(groupHero, TypeOption.HP, TypeOption.HP_PERCENT, TypeOption.HP_UNITS);
        atk = Util.calcValueTypeOption(groupHero, TypeOption.ATK, TypeOption.ATK_PERCENT, TypeOption.ATK_UNITS);
        def = Util.calcValueTypeOption(groupHero, TypeOption.DEF, TypeOption.DEF_PERCENT, TypeOption.DEF_UNITS);

        elm = Util.coefficientEm(groupHero);
        elm1 = Util.coefficientEm1(elm);
        elm2 = Util.coefficientEm2(elm);
        elm3 = Util.coefficientEm3(elm);

        critChance = Util.coefficientCritChance(groupHero);
        critDmg = Util.coefficientCritDmg(groupHero);

        //healingPercent
        //energyPercent

        firePercent = Util.calcElement(groupHero, TypeOption.FIRE_PERCENT);
        hydroPercent = Util.calcElement(groupHero, TypeOption.HYDRO_PERCENT);
        dendroPercent = Util.calcElement(groupHero, TypeOption.DENDRO_PERCENT);
        electroPercent = Util.calcElement(groupHero, TypeOption.ELECTRO_PERCENT);
        anemoPercent = Util.calcElement(groupHero, TypeOption.ANEMO_PERCENT);
        cryoPercent = Util.calcElement(groupHero, TypeOption.CRYO_PERCENT);
        geoPercent = Util.calcElement(groupHero, TypeOption.GEO_PERCENT);
        physPercent = Util.calcElement(groupHero, TypeOption.PHYS_PERCENT);


        vaporize = Util.calcReactionBonuses(groupHero, TypeElementStatus.VAPORIZE);
        burning = Util.calcReactionBonuses(groupHero, TypeElementStatus.BURNING);
        overloaded = Util.calcReactionBonuses(groupHero, TypeElementStatus.OVERLOADED);
        melt = Util.calcReactionBonuses(groupHero, TypeElementStatus.MELT);
        electroCharged = Util.calcReactionBonuses(groupHero, TypeElementStatus.ELECTRO_CHARGED);
        frozen = Util.calcReactionBonuses(groupHero, TypeElementStatus.FROZEN);
        superconduct = Util.calcReactionBonuses(groupHero, TypeElementStatus.SUPERCONDUCT);
        swirl = Util.calcReactionBonuses(groupHero, TypeElementStatus.SWIRL);
        crystallize = Util.calcReactionBonuses(groupHero, TypeElementStatus.CRYSTALLIZE);


    }
}
