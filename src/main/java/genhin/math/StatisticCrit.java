package genhin.math;

import lombok.Data;

import java.util.DoubleSummaryStatistics;

@Data
public class StatisticCrit {

    //Удачное срабатывание крита
    private double critCountOn = 0.0d;
    //количекство ударов
    private double attakCount = 0.0d;
    // critCountOn/ critCount
    private double critAverage = 0.0d;
    //Крит урон
    private double critDmg = 0.0d;
    //Крит урон Средний
    private double critDmgAverage = 0.0d;
    //крит Шанс
    private double critChance = 0.0d;


    //Статистика по рандому
    private DoubleSummaryStatistics summaryStatistics;

    public void critSumOn() {
        this.critCountOn++;

        this.critAverage = critCountOn == 0.0d ? 1.0d : critCountOn / attakCount;
        this.critDmgAverage = critDmg * critAverage;

    }


}
