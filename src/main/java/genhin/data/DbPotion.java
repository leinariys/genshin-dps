package genhin.data;

import genhin.json.TypeGroup;
import genhin.json.TypeOption;
import genhin.json.ValueOption;

import java.util.ArrayList;
import java.util.List;


/**
 * https://genshin-impact.fandom.com/wiki/Potions
 */
public class DbPotion {


    /**
     * Монолитное масло
     * Unmoving Essential Oil
     * Geo DMG by 25% for 300s.
     *
     * @return
     */
    public static List<ValueOption> unmovingEssentialOil() {
        return
                new ArrayList<>(
                        List.of(new ValueOption("Монолитное масло", TypeGroup.POTION, TypeOption.GEO_PERCENT, 25.0f, 300000.0f)
                        )
                );
    }

    //Dustproof Potion                                   Geo RES by 25% for 300s.
    //Gushing Essential Oil                                   Anemo DMG by 25% for 300s.
    //Windbarrier Potion                                   Anemo RES by 25% for 300s.
    //Streaming Essential Oil                                   Hydro DMG by 25% for 300s.
    //Desiccant Potion                                   Hydro RES by 25% for 300s.
    //Flaming Essential Oil                                   Pyro DMG by 25% for 300s.
    //Heatshield Potion                                   Pyro RES by 25% for 300s.
    //Frosting Essential Oil                                   Cryo DMG by 25% for 300s.
    //Frostshield Potion                                   Cryo RES by 25% for 300s.
    //Insulation Potion                                   Electro RES by 25% for 300s.


    /**
     * Громовое масло
     * Shocking Essential Oil
     * Electro DMG by 25% for 300s.
     *
     * @return
     */
    public static List<ValueOption> getShockingEssentialOil() {
        return
                new ArrayList<>(
                        List.of(new ValueOption("Громовое масло", TypeGroup.POTION, TypeOption.ELECTRO_PERCENT, 25.0f, 300000.0f)
                        )
                );
    }

}
