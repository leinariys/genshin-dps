package genhin.data;

import genhin.json.*;

import java.util.ArrayList;
import java.util.List;


/**
 * https://genshin-impact.fandom.com/wiki/Damage#Base_Enemy_Resistances
 */
public class DbEnemie {


    public static List<ValueOption> slimes() {
        return
                new ArrayList<>(
                        List.of(
                                //todo Остальные элементы
                                new ValueOption("Slimes", TypeGroup.CHARACTER, TypeOption.RES_PERCENT, 10.0f, TypeElement.ANEMO),
                                new ValueOption("Slimes", TypeGroup.CHARACTER, TypeOption.RES_PERCENT, 10.0f, TypeElement.PHYS)
                        )
                );
    }

    public static List<ValueOption> hilichurl() {
        return
                new ArrayList<>(
                        List.of(
                                //todo Остальные элементы
                                new ValueOption("Hilichurl", TypeGroup.CHARACTER, TypeOption.RES_PERCENT, 10.0f, TypeElement.ANEMO),
                                new ValueOption("Hilichurl", TypeGroup.CHARACTER, TypeOption.RES_PERCENT, 10.0f, TypeElement.PHYS)
                        )
                );
    }

    public static List<Enemies> getEnemies() {
        List<Enemies> enemies = new ArrayList<>();
        Enemies item;

        item = new Enemies("Slimes", 90);
        item.setOptions(slimes());
        enemies.add(item);

        item = new Enemies("Hilichurl", 90);
        item.setOptions(slimes());
        enemies.add(item);

        enemies.add(new Enemies("Mitachurl", 90));
        enemies.add(new Enemies("Samachurl", 90));
        enemies.add(new Enemies("Lawachurl", 90));
        enemies.add(new Enemies("Ruin Guard", 90));
        enemies.add(new Enemies("Ruin Grader", 90));
        enemies.add(new Enemies("Ruin Hunter", 90));
        enemies.add(new Enemies("Whopperflower", 90));
        enemies.add(new Enemies("Fatui Skirmisher", 90));
        enemies.add(new Enemies("Fatui Pyro Agent", 90));
        enemies.add(new Enemies("Fatui Cicin Mage", 90));
        enemies.add(new Enemies("Treasure Hoarders", 90));
        enemies.add(new Enemies("Millelith", 90));
        enemies.add(new Enemies("Geovishap Hatchling", 90));
        enemies.add(new Enemies("Abyss Mages", 90));
        enemies.add(new Enemies("Eye of the Storm", 90));
        enemies.add(new Enemies("Cicin", 90));
        enemies.add(new Enemies("The Great Snowboar King", 90));
        enemies.add(new Enemies("Hypostasis", 90));
        enemies.add(new Enemies("Oceanid", 90));
        enemies.add(new Enemies("Regisvine", 90));
        enemies.add(new Enemies("Dvalin", 90));
        enemies.add(new Enemies("Andrius", 90));
        enemies.add(new Enemies("Tartaglia", 90));
        enemies.add(new Enemies("Tartaglia", 90));
        enemies.add(new Enemies("Tartaglia", 90));

        return enemies;
    }


}
