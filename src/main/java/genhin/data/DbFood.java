package genhin.data;

import genhin.json.TypeGroup;
import genhin.json.TypeOption;
import genhin.json.ValueOption;

import java.util.ArrayList;
import java.util.List;


/**
 * https://genshin-impact.fandom.com/wiki/Food
 */
public class DbFood {


    /**
     * Нефритовые посылки
     * Jade Parcels
     * Увеличивает Атаку всех членов группы на 224 ~ 320 и шанс Критического удара на 6 ~ 10 % на 300 секунд.
     *
     * @return
     */
    public static List<ValueOption> JadeParcels(boolean isQuality) {
        return isQuality ?
                new ArrayList<>(
                        List.of(
                                new ValueOption("Монолитное масло", TypeGroup.POTION, TypeOption.ATK_UNITS, 320.0f, 300000.0f),
                                new ValueOption("Монолитное масло", TypeGroup.POTION, TypeOption.CRIT_PERCENT, 10.0f, 300000.0f)
                        )
                )

                :
                new ArrayList<>(
                        List.of(
                                new ValueOption("Монолитное масло", TypeGroup.POTION, TypeOption.ATK_UNITS, 224.0f, 300000.0f),
                                new ValueOption("Монолитное масло", TypeGroup.POTION, TypeOption.CRIT_PERCENT, 6.0f, 300000.0f)
                        )
                )
                ;
    }


}
