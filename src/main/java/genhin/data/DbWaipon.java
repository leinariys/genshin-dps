package genhin.data;

import genhin.json.*;

import java.util.ArrayList;
import java.util.List;

public class DbWaipon {


    /**
     * Аквила Фавония
     * Aquila Favonia
     * Вторичный: Physical DMG 41.3%
     * Базовая ATK:674
     * Атака увеличена на 40% . Срабатывает при получении Урона:
     * восстанавливая HP, равное 160% от Атаки,
     * и нанося 320% от Атаки в качестве Урона окружающим врагам.
     * Этот эффект может срабатывать только раз в 15 секунд.
     *
     * @return
     */
    public static List<ValueOption> aquilaFavonia() {
        return
                new ArrayList<>(
                        List.of(
                                new ValueOption("Aquila Favonia Secondary", TypeGroup.WEAPON, TypeOption.PHYS_PERCENT, 41.3f),
                                new ValueOption("Aquila Favonia Base", TypeGroup.WEAPON, TypeOption.ATK, 674.0f),
                                new ValueOption("Aquila Favonia", TypeGroup.WEAPON, TypeOption.ATK_PERCENT, 40.0f),
                                new ValueOption("Aquila Favonia Ability", TypeGroup.WEAPON, TypeAbility.WAIPON, TypeElement.PHYS, 320.0f, 15000.0f)
                        )
                );
    }


    /**
     * Черногорский длинный меч
     * Blackcliff Longsword
     * Вторичный: Критический урон 36,8%
     * Базовая АТК: 565
     * После победы над противником Атака увеличивается на 24% на 30 секунд.
     * Этот эффект имеет максимум 3 стека, и продолжительность каждого стака не зависит от других.
     *
     * @return
     */
    public static List<ValueOption> getBlackcliffLongsword() {
        return
                new ArrayList<>(
                        List.of(
                                new ValueOption("Blackcliff Longsword Secondary", TypeGroup.WEAPON, TypeOption.CRYO_PERCENT, 36.8f),
                                new ValueOption("Blackcliff Longsword Base", TypeGroup.WEAPON, TypeOption.ATK, 565.0f)
                                //, new ValueOption("Blackcliff Longsword", TypeGroup.WEAPON, TypeOption.ATK_PERCENT, 24.0f)
                        )
                );
    }

}
