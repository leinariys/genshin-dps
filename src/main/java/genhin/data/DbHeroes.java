package genhin.data;


import genhin.json.TypeGroup;
import genhin.json.TypeOption;
import genhin.json.ValueOption;

import java.util.ArrayList;
import java.util.List;

/**
 * База по героям
 */
public class DbHeroes {

    /**
     * Stat 90
     *
     * @return
     */
    public static List<ValueOption> getKeqing() {
        return
                new ArrayList<>(
                        List.of(
                                new ValueOption(TypeGroup.CHARACTER, TypeOption.HP, 13103.0f),
                                new ValueOption(TypeGroup.CHARACTER, TypeOption.ATK, 323.0f),
                                new ValueOption(TypeGroup.CHARACTER, TypeOption.DEF, 799.0f),
                                new ValueOption(TypeGroup.CHARACTER, TypeOption.CRIT_CHANCE_PERCENT, 5.0f),
                                new ValueOption(TypeGroup.CHARACTER, TypeOption.CRIT_PERCENT, 88.4f)
                        )
                );
    }

}
