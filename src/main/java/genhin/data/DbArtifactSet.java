package genhin.data;

import genhin.json.TypeElementStatus;
import genhin.json.TypeGroup;
import genhin.json.TypeOption;
import genhin.json.ValueOption;

import java.util.ArrayList;
import java.util.List;

/**
 * https://genshin-impact.fandom.com/wiki/Artifacts
 */
public class DbArtifactSet {


    /**
     * Thundering Fury
     * Громовой рёв ярости
     * 2 Item: Бонус Electro DMG + 15%
     * 4 Item: Увеличивает урон от перегрузок, электрического заряда и сверхпроводимости на 40%.
     * Срабатывание таких эффектов снижает время восстановления умения стихий на 1 секунду.
     * Может срабатывать только раз в 0,8 с.
     *
     * @return
     */
    public static List<ValueOption> getThunderingFury() {
        return
                new ArrayList<>(
                        List.of(
                                new ValueOption("Thundering Fury 2 Item", TypeGroup.ARTIFACT, TypeOption.ELECTRO_PERCENT, 15.0f),

                                new ValueOption("Thundering Fury 4 Item", TypeGroup.ARTIFACT, TypeElementStatus.OVERLOADED, 40.0f, 800.0f),
                                new ValueOption("Thundering Fury 4 Item", TypeGroup.ARTIFACT, TypeElementStatus.ELECTRO_CHARGED, 40.0f, 800.0f),
                                new ValueOption("Thundering Fury 4 Item", TypeGroup.ARTIFACT, TypeElementStatus.SUPERCONDUCT, 40.0f, 800.0f),

                                //Цветок
                                new ValueOption("Цветок", TypeGroup.ARTIFACT, 1, TypeOption.HP_UNITS, 4780.0f),
                                new ValueOption("Цветок", TypeGroup.ARTIFACT, 1, TypeOption.ATK_UNITS, 35.0f),
                                new ValueOption("Цветок", TypeGroup.ARTIFACT, 1, TypeOption.ATK_PERCENT, 16.3f),
                                new ValueOption("Цветок", TypeGroup.ARTIFACT, 1, TypeOption.CRIT_CHANCE_PERCENT, 3.1f),
                                new ValueOption("Цветок", TypeGroup.ARTIFACT, 1, TypeOption.ENERGY_PERCENT, 11.7f),
                                //Перо
                                new ValueOption("Перо", TypeGroup.ARTIFACT, 2, TypeOption.ATK_UNITS, 311.0f),
                                new ValueOption("Перо", TypeGroup.ARTIFACT, 2, TypeOption.DEF_UNITS, 62.0f),
                                new ValueOption("Перо", TypeGroup.ARTIFACT, 2, TypeOption.ATK_PERCENT, 9.9f),
                                new ValueOption("Перо", TypeGroup.ARTIFACT, 2, TypeOption.CRIT_PERCENT, 10.9f),
                                new ValueOption("Перо", TypeGroup.ARTIFACT, 2, TypeOption.HP_UNITS, 299.f),
                                //Часы
                                new ValueOption("Часы", TypeGroup.ARTIFACT, 3, TypeOption.ATK_PERCENT, 46.6f),
                                new ValueOption("Часы", TypeGroup.ARTIFACT, 3, TypeOption.ATK_UNITS, 47.0f),
                                new ValueOption("Часы", TypeGroup.ARTIFACT, 3, TypeOption.CRIT_CHANCE_PERCENT, 7.4f),
                                new ValueOption("Часы", TypeGroup.ARTIFACT, 3, TypeOption.ENERGY_PERCENT, 9.1f),
                                new ValueOption("Часы", TypeGroup.ARTIFACT, 3, TypeOption.CRIT_PERCENT, 5.4f),
                                //Кубок
                                new ValueOption("Кубок", TypeGroup.ARTIFACT, 4, TypeOption.ATK_PERCENT, 46.6f),
                                new ValueOption("Кубок", TypeGroup.ARTIFACT, 4, TypeOption.DEF_UNITS, 46.0f),
                                new ValueOption("Кубок", TypeGroup.ARTIFACT, 4, TypeOption.CRIT_PERCENT, 10.9f),
                                new ValueOption("Кубок", TypeGroup.ARTIFACT, 4, TypeOption.HEALING_PERCENT, 4.7f),
                                new ValueOption("Кубок", TypeGroup.ARTIFACT, 4, TypeOption.ATK_UNITS, 45.0f),
                                //Корона
                                new ValueOption("Корона", TypeGroup.ARTIFACT, 5, TypeOption.CRIT_PERCENT, 62.2f),
                                new ValueOption("Корона", TypeGroup.ARTIFACT, 5, TypeOption.DEF_PERCENT, 6.6f),
                                new ValueOption("Корона", TypeGroup.ARTIFACT, 5, TypeOption.ELM, 42.0f),
                                new ValueOption("Корона", TypeGroup.ARTIFACT, 5, TypeOption.ATK_PERCENT, 14.0f),
                                new ValueOption("Корона", TypeGroup.ARTIFACT, 5, TypeOption.ENERGY_PERCENT, 12.3f)

                        )
                );
    }


}
