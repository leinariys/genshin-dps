package genhin.data;

import genhin.json.TypeAbility;
import genhin.json.TypeElement;
import genhin.json.TypeGroup;
import genhin.json.ValueOption;

import java.util.ArrayList;
import java.util.List;


/**
 * https://genshin-impact.fandom.com/wiki/Characters
 */
public class DbConstellation {


    /**
     * Громовая Мощь
     * Thundering Might
     * Повторное использование звездного восстановления при наличии Lightning Stiletto
     * заставляет Кецин наносить 50% своей атаки в виде AoE Electro DMG в начальной точке и в конце ее Blink.
     *
     * @return
     */
    public static List<ValueOption> getThunderingMight() {
        return
                new ArrayList<>(
                        List.of(
                                new ValueOption("Громовая Мощь", TypeGroup.CONSTELLATION, TypeElement.ELECTRO, TypeAbility.CONSTELLATION, 50.0f)
                        )
                );
    }


}
