package genhin;

import genhin.app.LogicApp;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;


@Slf4j
public class ApplicationDpsMeter extends Application {

    ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void init() {
        log.debug("Init");
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        log.debug("Start");
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        Parent root = FXMLLoader.load(getClass().getResource("/fxml/mainWindow.fxml"));
        primaryStage.setTitle("Genshin DPS");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
        System.out.println("Start");

        LogicApp.setStage(primaryStage);
        LogicApp.initFile();



        // executorService.scheduleWithFixedDelay(LogicApp.INSTANCE.run(), 1,5, TimeUnit.SECONDS);
    }


    @Override
    public void stop() {
        executorService.shutdown();
        log.debug("Stop");
    }

}


