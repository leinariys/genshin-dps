package genhin.app;

import genhin.json.DpsMetrConteiner;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.File;

@Data
@Getter
public class Configuration {
    @Getter
    private static String version = "1.5";
    @Getter
    private static String author = "Leinariys";
    @Getter
    private static String contactForCommunication = "@Leinariys";
    private static String pathHeroeName = "account-heroe-list.json";
    @Getter
    @Setter
    private static File fileHeroeList = new File(pathHeroeName);
    @Getter
    @Setter
    private static DpsMetrConteiner dpsMetrConteiner = new DpsMetrConteiner();


}
