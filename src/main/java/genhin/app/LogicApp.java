package genhin.app;


import genhin.Util;
import genhin.app.Configuration;
import genhin.controller.ItemOption;
import genhin.controller.TitledPaneAbility;
import genhin.json.*;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.io.IOException;
import java.util.Optional;


@Slf4j
@Data

public class LogicApp {
    private static JFileChooser chooser = new JFileChooser();
    private static FileFilter filter = new FileNameExtensionFilter("Hero json", "json");

    private static Stage primaryStage;
    private static TreeItem<NadaTree> treeGroup;
    private static ObservableList<FlowPane> listValueOption;
    private static Accordion accordionGroup;

    public static void setStage(Stage stage) {
        primaryStage = stage;
    }

    public static void setTreeGroup(TreeItem<NadaTree> eventTarget) {
        treeGroup = eventTarget;
    }

    public static void setListValueOption(ObservableList<FlowPane> observable) {
        listValueOption = observable;
    }

    public static void setAccordionGroup(Accordion eventTarget) {
        accordionGroup = eventTarget;
    }

    /**
     * Проверка наличия файла и создание нового если нет
     */
    public static void initFile() {
        if (Configuration.getFileHeroeList().exists()) {
            Configuration.setDpsMetrConteiner(Util.reader(Configuration.getFileHeroeList()));
        } else {
            Util.write(Configuration.getDpsMetrConteiner(), Configuration.getFileHeroeList());
        }
        primaryStage.setTitle(String.format("Genshin DPS - [%s]", Configuration.getFileHeroeList().getName()));
        updateUI();
    }

    public static void menuBarNewEvent() {
        Configuration.setDpsMetrConteiner(new DpsMetrConteiner());
        updateUI();
    }

    public static void menuBarOpenEvent() {
        chooser.setDialogTitle("Выбрать файл с Персонажами ");
        chooser.setSelectedFile(Configuration.getFileHeroeList());
        chooser.addChoosableFileFilter(filter);
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

        int status = chooser.showOpenDialog(null);
        switch (status) {
            case JFileChooser.APPROVE_OPTION:
                Configuration.setFileHeroeList(    chooser.getSelectedFile()   );
                if (Configuration.getFileHeroeList() != null) {
                    System.out.println(Configuration.getFileHeroeList());
                    Util.write(Configuration.getDpsMetrConteiner(), Configuration.getFileHeroeList());
                    updateUI();
                }
                break;
        }
    }

    public static void menuBarSaveEvent() {
        chooser.setDialogTitle("Сохранить файл с Персонажами ");
        chooser.setSelectedFile(Configuration.getFileHeroeList());
        chooser.addChoosableFileFilter(filter);
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

        int status = chooser.showOpenDialog(null);
        switch (status) {
            case JFileChooser.APPROVE_OPTION:
                Configuration.setFileHeroeList(    chooser.getSelectedFile()   );
                if (Configuration.getFileHeroeList() != null) {
                    System.out.println(Configuration.getFileHeroeList());
                    Util.write(Configuration.getDpsMetrConteiner(), Configuration.getFileHeroeList());
                    updateUI();
                }
                break;
        }
    }

    public static void menuBarAbout() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText("Разработал  Я");
        alert.setContentText("Работает так себе, но вы пользуйтесь ");
        alert.showAndWait();
    }

    public static GroupHero addNewGroupHero(String name) {
        GroupHero grouphero = new GroupHero(name);
        Configuration.getDpsMetrConteiner().setGroupCharacter(grouphero);
        return grouphero;
    }

    public static Hero addNewHero(String name, String idGroupHero) {
        Hero grouphero = new Hero(name);
        Optional<GroupHero> optionalGroupHero = Configuration.getDpsMetrConteiner().getParty().stream().filter(f -> f.getId().equals(idGroupHero)).findFirst();
        optionalGroupHero.get().setCharact(grouphero);
        return grouphero;
    }

    public static ValueOption addNewValueOption(String idGroupHero, String idHero) {
        ValueOption valueOption = new ValueOption();
        Optional<Hero> optionalGroupHero = Configuration.getDpsMetrConteiner().getParty().stream()
                .filter(f -> f.getId().equals(idGroupHero)).findFirst().get().getHeroes().stream()
                .filter(f -> f.getId().equals(idHero)).findFirst();
        optionalGroupHero.get().setValueOption(valueOption);
        return valueOption;
    }

    public static void menuLog() {
        Util.write(Configuration.getDpsMetrConteiner(), Configuration.getFileHeroeList());
    }

    public static void updateUI() {
        primaryStage.setTitle(String.format("Genshin DPS - [%s]", Configuration.getFileHeroeList().getName()));

        TreeItem<NadaTree> newTreeHero = null;
        for (GroupHero groupHero : Configuration.getDpsMetrConteiner().getParty()) {
            TreeItem<NadaTree> group = new TreeItem<NadaTree>(groupHero);
            treeGroup.getChildren().add(group);
            accordionGroup.getPanes().add(LogicApp.newItemPaneAbility(groupHero));
            for (Hero hero : groupHero.getHeroes()) {
                newTreeHero = new TreeItem<NadaTree>(hero);
                group.getChildren().add(newTreeHero);
                group.setExpanded(true);
            }
        }

        updateUIlist(newTreeHero);
    }

    public static void updateUIlist(TreeItem<NadaTree> newTreeHero) {

        if (newTreeHero != null) {
            NadaTree nadaTree = newTreeHero.getValue();

            if (nadaTree instanceof GroupHero) {
                System.out.println("GroupHero:" + (GroupHero) nadaTree);
            }

            if (nadaTree instanceof Hero) {
                System.out.println("Hero:" + (Hero) nadaTree);
                Hero heroInstanceof = (Hero) nadaTree;
                listValueOption.clear();
                for (ValueOption item : heroInstanceof.getOptions()) {
                    listValueOption.add(LogicApp.newItemOption(item));
                }
            }
        }
    }


    public static FlowPane newItemOption(ValueOption itemValueOption) {
        try {
            FXMLLoader itemXml = new FXMLLoader(LogicApp.class.getResource("/fxml/itemOption.fxml"));
            FlowPane itemFlowPane = itemXml.load();
            ItemOption itemController = itemXml.getController();
            itemController.init(itemValueOption);

            return itemFlowPane;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static TitledPane newItemPaneAbility(GroupHero itemGroupHero) {
        try {
            FXMLLoader itemXml = new FXMLLoader(LogicApp.class.getResource("/fxml/titledPaneAbility.fxml"));
            TitledPane itemTitledPane = itemXml.load();
            TitledPaneAbility itemController = itemXml.getController();
            itemController.init(itemGroupHero);

            return itemTitledPane;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static void menuBarVersion() {
        Configuration.getDpsMetrConteiner()
                .setVersion(
                        LogicApp.showDialogEditor(
                                "Новая/Edit Version",
                                "Задайте Version",
                                "Version:",
                                Configuration.getDpsMetrConteiner().getVersion()
                        )
                );
    }

    public static void menuBarAuthor() {
        Configuration.getDpsMetrConteiner()
                .setAuthor(
                        LogicApp.showDialogEditor(
                                "Новая/Edit Author",
                                "Задайте Author",
                                "Author:",
                                Configuration.getDpsMetrConteiner().getAuthor()
                        )
                );
    }

    public static void menuBarContact() {
        Configuration.getDpsMetrConteiner()
                .setContact(
                        LogicApp.showDialogEditor(
                                "Новая/Edit Contact",
                                "Задайте Contact",
                                "Contact:",
                                Configuration.getDpsMetrConteiner().getContact()
                        )
                );
    }


    public static String showDialogEditor(String title, String headerText, String contentText, String editorText) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle(title);
        dialog.setHeaderText(headerText);
        dialog.setContentText(contentText);
        dialog.getEditor().setText(editorText);
        Optional<String> result = dialog.showAndWait();
        return result.get();
    }


    public Runnable run() {
        Runnable run = new Runnable() {
            @Override
            public void run() {
                System.out.println("LogicApp run Runnable");
            }
        };
        return run;
    }

}
