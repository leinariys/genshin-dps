package genhin.json;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class NadaTree {

    /*ИД для UI*/
    protected String id;


    protected String name;

    public NadaTree() {
    }

    public NadaTree(String name) {
        this.name = name;
    }

    public String getId() {
        if (id == null ) {
            return Integer.toHexString(hashCode());
        }else {
            return this.id;
        }
    }

    @Override
    public String toString() {
        return this.name == null || this.name.isEmpty() ?"-Пусто-" :this.name; // getClass().getName() + "@" + Integer.toHexString(hashCode());
    }


}
