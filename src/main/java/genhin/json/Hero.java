package genhin.json;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


//https://genshin-impact.fandom.com/wiki/Damage

/**
 * Персонаж
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Hero extends NadaTree {

    public Hero() {
    }

    public Hero(String name) {
        this.name = name;
    }

    private int lvl;

    private boolean isSelect;

    private List<ValueOption> options = new ArrayList<ValueOption>();

    @JsonIgnore
    public void setValueOption(ValueOption... valueOption) {
        options.addAll(Arrays.asList(valueOption));
    }

    @JsonIgnore
    public void setValueOption(List<ValueOption> valueOption) {
        options.addAll(valueOption);
    }

    @Override
    public String toString() {
        return String.format("%s [%s] %s", name, lvl, isSelect ? "☒" : "☐"); // getClass().getName() + "@" + Integer.toHexString(hashCode());
    }
}
