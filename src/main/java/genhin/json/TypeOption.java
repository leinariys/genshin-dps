package genhin.json;

public enum TypeOption {
    NULL,
    HP,
    ATK,
    DEF,
    //  RES ,
    ELM,
    HP_PERCENT,
    ATK_PERCENT,
    DEF_PERCENT,
    RES_PERCENT,
    ENERGY_PERCENT,

    //Числа
    HP_UNITS,
    ATK_UNITS,
    DEF_UNITS,

    //  DMG_BONUS
    FIRE_PERCENT,
    HYDRO_PERCENT,
    DENDRO_PERCENT,
    ELECTRO_PERCENT,
    ANEMO_PERCENT,
    CRYO_PERCENT,
    GEO_PERCENT,
    PHYS_PERCENT,


    CRIT_PERCENT,
    CRIT_CHANCE_PERCENT,

    HEALING_PERCENT,


    RUN_TALENT_ATTACK,
    ;//рейзор

    public static TypeOption getType(String value) {
        for (TypeOption v : values()) {
            if (v.name().equals(value)) {
                return v;
            }
        }
        return null;
    }
}
