package genhin.json;

/**
 * Тип умения/способности (ability):  Таланта/оружие/Реакт.Перегрузка/Созвездие
 */
public enum TypeAbility {
    NULL,
    NOT,
    ATTACK,
    ATTACK_AIM,
    ATTACK_CHARGED,
    ATTACK_FALL,
    //Высокая атака
    ATTACK_UP,
    //Низкая Атака
    ATTACK_DOWN,
    E,
    Q,
    WAIPON,//Окосознания
    ELM,//Перегрузка
    TALENT,//Рейзор ульта
    CONSTELLATION,//Рейзор 6
}
