package genhin.json;


/**
 * Тип выходящего урона
 * Сахароза с руки Анемо
 */
public enum TypeElement {
    NULL,
    FIRE,
    HYDRO,
    DENDRO,
    ELECTRO,
    ANEMO,
    CRYO,
    GEO,
    PHYS,
}
