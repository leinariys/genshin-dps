package genhin.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DpsMetrConteiner {

    private String version;
    private String author;
    private String contact;

    private List<GroupHero> party = new ArrayList<>();
    private List<Enemies> enemies = new ArrayList<>();


    public void setGroupCharacter(GroupHero... groupCharacters) {
        for (GroupHero item : groupCharacters) {
            this.party.add(item);
        }
    }

}
