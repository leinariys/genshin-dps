package genhin.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

//   https://genshin-impact.fandom.com/wiki/Elements
//   https://genshin-impact.fandom.com/wiki/Attributes
//   https://genshin-impact.fandom.com/wiki/Artifacts
//   https://genshin-impact.fandom.com/wiki/Damage
//   https://ngabbs.com/read.php?tid=23633102&rand=32

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ValueOption extends NadaTree {
    /**
     * Группа значений CHARACTER, WEAPON, ARTIFACT, CONSTELLATION, TALENT
     */

    private TypeGroup typeGroup = TypeGroup.NULL;

    /**
     * CHARACTER, WEAPON, ARTIFACT, CONSTELLATION - тип значения статов
     * POTION - Увеличение урона
     * TALENT для не TypeAbility.NOT
     * Зависимость TypeAbility от Тип харастеристики
     * Таланта урон от защиты или хп
     */

    private TypeOption typeOption = TypeOption.NULL;

    /**CHARACTER, WEAPON, ARTIFACT, CONSTELLATION, TALENT - тип значения статов
     * Тип значения в формуле
     */
    //  @Getter
    //  private TypeFormula typeFormula;


    /**
     * TALENT
     * Тим таланта	0
     * Пасивка
     * Атака
     * Атака прицельный
     * Атака заряженная
     * Атака в падении
     * Атака Низко/высоко
     * Элементальный навык
     * Взрыв стихий
     * <p>
     * Использовать с {@link TypeElement} обязательно
     * <p>
     * То что активируется или переодически наносит урон {@link TypeAbility#ATTACK}
     * Тип умения/способности (ability):  Таланта/оружие/Реакт.Перегрузка/Созвездие
     */

    private TypeAbility typeAbility = TypeAbility.NULL;


    /**
     * Нанесённая стихия исползовать с {@link TypeAbility} обязательно
     * Enemies - реакция на мобе
     * TALENT
     * Тип выходящего урона
     * Сахароза с руки Анемо
     */

    private TypeElement damageTypeElement = TypeElement.NULL;

    /**
     * Элементальный статус
     * ARTIFACT
     * Тип увеличиваемого бонуса реакции
     */

    private TypeElementStatus typeElementStatus = TypeElementStatus.NULL;


    /**
     * Влияние на всю группу
     */

    private boolean isAffectsGroup;

    /**
     * комбо	0
     * 1-9
     * группа в комбе	0
     * 1-9
     * ARTIFACT
     * Корона 1
     * Перо 2
     * Часы 3
     * Кубок 4
     * Шляпа 5
     */

    private int index;

    /**
     * Основной параметр
     * <p>
     * TALENT
     * Параметр для умножения
     * TypeChar.ATK * valueMult
     */

    private double valueMain = 1.0d;
    /**
     * TALENT
     * Доп. Параметр для умножения
     * TypeChar.ATK * valueMult + TypeChar.ATK * valueMult2
     */

    private double valueMult2 = 0.0d;

    /**
     * TALENT
     * Доп. Параметр для сложения
     * TypeChar.HP * valueMult + valueAdd
     */

    private double valueAdd = 0.0d;

    /**
     * TALENT
     * Тики атаки * valueMult
     */

    private double teak = 1.0d;

    /**
     * POTION - время воздействия
     * TALENT - Время проигрывания анимации
     */

    private double timeAniMs = 0.0d;

    /**
     * WAIPON - Время отката
     */

    private double timeRollbackMs = 0.0d;

    public ValueOption() {
    }

    /**
     * @param typeGroup
     * @param typeOption
     * @param valueMain
     */
    public ValueOption(TypeGroup typeGroup, TypeOption typeOption, double valueMain) {
        this.typeGroup = typeGroup;
        this.typeOption = typeOption;
        this.valueMain = valueMain;
    }

    public ValueOption(TypeGroup typeGroup, TypeOption typeOption, double valueMain, boolean isAffectsGroup) {
        this.typeGroup = typeGroup;
        this.typeOption = typeOption;
        this.valueMain = valueMain;
        this.isAffectsGroup = isAffectsGroup;
    }

    /**
     * Enemy Резисты врагов
     *
     * @param typeGroup
     * @param typeOption
     * @param valueMain
     * @param elementalRes
     */
    public ValueOption(String name, TypeGroup typeGroup, TypeOption typeOption, float valueMain, TypeElement elementalRes) {
        this.name = name;
        this.typeGroup = typeGroup;
        this.typeOption = typeOption;
        this.valueMain = valueMain;
        this.damageTypeElement = elementalRes;
    }

    /**
     * Конструктор Зелья
     *
     * @param name
     * @param typeGroup
     * @param typeOption
     * @param valueMain
     * @param timeAnimationMs
     */
    public ValueOption(String name, TypeGroup typeGroup, TypeOption typeOption, float valueMain, float timeAnimationMs) {
        this.name = name;
        this.typeGroup = typeGroup;
        this.typeOption = typeOption;
        this.valueMain = valueMain;
        this.timeAniMs = timeAnimationMs;
    }

    /**
     * Статы оружия, Артифакта
     *
     * @param name
     * @param typeGroup
     * @param typeOption
     * @param valueMain
     */
    public ValueOption(String name, TypeGroup typeGroup, TypeOption typeOption, float valueMain) {
        this.name = name;
        this.typeGroup = typeGroup;
        this.typeOption = typeOption;
        this.valueMain = valueMain;
    }

    /**
     * Оружие независимая атака
     *
     * @param name
     * @param typeGroup
     * @param typeAbility
     * @param valueMain
     * @param timeRollbackMs
     */
    public ValueOption(String name, TypeGroup typeGroup, TypeAbility typeAbility, TypeElement damageTypeElement, float valueMain, float timeRollbackMs) {
        this.name = name;
        this.typeGroup = typeGroup;
        this.typeAbility = typeAbility;
        this.damageTypeElement = damageTypeElement;
        this.valueMain = valueMain;
        this.timeRollbackMs = timeRollbackMs;
    }

    /**
     * Сет артифактов  4 Item
     *
     * @param name
     * @param typeGroup
     * @param typeElementStatus
     * @param valueMain
     * @param timeRollbackMs
     */
    public ValueOption(String name, TypeGroup typeGroup, TypeElementStatus typeElementStatus, float valueMain, float timeRollbackMs) {
        this.name = name;
        this.typeGroup = typeGroup;
        this.typeElementStatus = typeElementStatus;
        this.valueMain = valueMain;
        this.timeRollbackMs = timeRollbackMs;

    }

    /**
     * Артифайкты  статы
     *
     * @param name
     * @param typeGroup
     * @param index
     * @param typeOption
     * @param valueMain
     */
    public ValueOption(String name, TypeGroup typeGroup, int index, TypeOption typeOption, float valueMain) {
        this.name = name;
        this.typeGroup = typeGroup;
        this.index = index;
        this.typeOption = typeOption;
        this.valueMain = valueMain;
    }

    /**
     * Созвездие
     *
     * @param name
     * @param typeGroup
     * @param damageTypeElement
     * @param typeAbility
     * @param valueMain
     */
    public ValueOption(String name, TypeGroup typeGroup, TypeElement damageTypeElement, TypeAbility typeAbility, float valueMain) {
        this.name = name;
        this.typeGroup = typeGroup;
        this.damageTypeElement = damageTypeElement;
        this.typeAbility = typeAbility;
        this.valueMain = valueMain;
    }


    /**
     * TalentAttack Combo - Атака обычная
     *
     * @param name
     * @param typeGroup
     * @param typeOption        Параметр от которого расчитывается атака
     * @param damageTypeElement
     * @param typeAbility
     * @param index
     * @param valueMain
     * @param timeAniMs
     */
    public ValueOption(String name, TypeGroup typeGroup, TypeOption typeOption, TypeElement damageTypeElement, TypeAbility typeAbility, int index, float valueMain, float timeAniMs) {
        this.name = name;
        this.typeGroup = typeGroup;
        this.typeOption = typeOption;
        this.damageTypeElement = damageTypeElement;
        this.typeAbility = typeAbility;
        this.index = index;
        this.valueMain = valueMain;
        this.timeAniMs = timeAniMs;

    }

    /**
     * TalentAttack Combo - Атака обычная
     *
     * @param name
     * @param typeGroup
     * @param typeOption        Параметр от которого расчитывается атака
     * @param damageTypeElement
     * @param typeAbility
     * @param index
     * @param valueMain
     * @param valueMult2
     * @param timeAniMs
     */
    public ValueOption(String name, TypeGroup typeGroup, TypeOption typeOption, TypeElement damageTypeElement, TypeAbility typeAbility, int index, float valueMain, float valueMult2, float timeAniMs) {
        this.name = name;
        this.typeGroup = typeGroup;
        this.typeOption = typeOption;
        this.damageTypeElement = damageTypeElement;
        this.typeAbility = typeAbility;
        this.index = index;
        this.valueMain = valueMain;
        this.valueMult2 = valueMult2;
        this.timeAniMs = timeAniMs;


    }

    /**
     * AttackCharged - Атака заряженная
     *
     * @param name
     * @param typeGroup
     * @param typeOption        Параметр от которого расчитывается атака
     * @param damageTypeElement
     * @param typeAbility
     * @param valueMain
     * @param valueMult2
     * @param timeAniMs
     */
    public ValueOption(String name, TypeGroup typeGroup, TypeOption typeOption, TypeElement damageTypeElement, TypeAbility typeAbility, float valueMain, float valueMult2, float timeAniMs) {
        this.name = name;
        this.typeGroup = typeGroup;
        this.typeOption = typeOption;
        this.damageTypeElement = damageTypeElement;
        this.typeAbility = typeAbility;
        this.valueMain = valueMain;
        this.valueMult2 = valueMult2;
        this.timeAniMs = timeAniMs;
    }

    /**
     * TalentE - Элементальный навык
     * TalentQ - Взрыв стихий
     *
     * @param name
     * @param typeGroup
     * @param typeOption        Параметр от которого расчитывается атака
     * @param damageTypeElement
     * @param teak
     * @param typeAbility
     * @param valueMain
     * @param timeAniMs
     */
    public ValueOption(String name, TypeGroup typeGroup, TypeOption typeOption, TypeElement damageTypeElement, float teak, TypeAbility typeAbility, float valueMain, float timeAniMs) {
        this.name = name;
        this.typeGroup = typeGroup;
        this.typeOption = typeOption;
        this.damageTypeElement = damageTypeElement;
        this.typeAbility = typeAbility;
        this.valueMain = valueMain;
        this.teak = teak;
        this.timeAniMs = timeAniMs;
    }

    /**
     * AttackFall - Атака в падении
     * AttackUpDown - Атака Низко/высоко
     * TalentE - Элементальный навык
     * TalentQ - Взрыв стихий
     *
     * @param name
     * @param typeGroup
     * @param typeOption        Параметр от которого расчитывается атака
     * @param damageTypeElement
     * @param typeAbility
     * @param valueMain
     * @param timeAniMs
     */
    public ValueOption(String name, TypeGroup typeGroup, TypeOption typeOption, TypeElement damageTypeElement, TypeAbility typeAbility, float valueMain, float timeAniMs) {
        this.name = name;
        this.typeGroup = typeGroup;
        this.typeOption = typeOption;
        this.damageTypeElement = damageTypeElement;
        this.typeAbility = typeAbility;
        this.valueMain = valueMain;
        this.timeAniMs = timeAniMs;
    }


}
