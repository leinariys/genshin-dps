package genhin.json;


/**
 * Тип выходящего урона
 * Сахароза с руки Анемо
 */
public enum TypeElementStatus {
    NULL,

    /**
     * ИСПАРИТЬ
     */
    VAPORIZE,
    /**
     * ГОРЯТЬ
     */
    BURNING,
    /**
     * ПЕРЕГРУЗИТЬ
     */
    OVERLOADED,
    /**
     * РАСПЛАВИТЬ
     */
    MELT,
    /**
     * ЭЛЕКТРО_ЗАРЯД
     */
    ELECTRO_CHARGED,
    /**
     * ЗАМОРОЗИТЬ
     */
    FROZEN,
    /**
     * СУПЕРПРОВОДИТЬ
     */
    SUPERCONDUCT,
    /**
     * ВЕРНУТЬ
     */
    SWIRL,
    /**
     * КРИСТАЛЛИЗИРОВАТЬ
     */
    CRYSTALLIZE,
}
