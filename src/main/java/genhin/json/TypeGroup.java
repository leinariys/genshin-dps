package genhin.json;

/**
 * Группа значений CHARACTER, WEAPON, ARTIFACT, CONSTELLATION, TALENT
 */
public enum TypeGroup {
    NULL,
    CHARACTER,
    WEAPON,
    ARTIFACT,
    CONSTELLATION,
    TALENT,
    //Еда
    FOOD,
    //Зелья
    POTION,
    //Элементальная реакция
    ELEMENT,

    ;

}
