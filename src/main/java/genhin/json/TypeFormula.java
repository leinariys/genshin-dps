package genhin.json;

public enum TypeFormula {

    HERO,
    WEAPON,
    ATK_PERCENT,
    ATK_UNITS,

    ABILITY,
    DMG_BONUS,
    RES,
    DEF,
    EM,
    CRIT_DMG,
    CRIT_RATE,

}
