package genhin.json.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


/**
 * Gати персов
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AvatarSkillExcelConfigData {

    private String NameTextMapHashString;
    private String DescTextMapHashString;

    @JsonProperty("Id")
    private Integer Id;
    @JsonProperty("NameTextMapHash")
    private Long NameTextMapHash;
    @JsonProperty("AbilityName")
    private String AbilityName;
    @JsonProperty("DescTextMapHash")
    private Long DescTextMapHash;
    @JsonProperty("SkillIcon")
    private String SkillIcon;
    @JsonProperty("CostStamina")
    private Double CostStamina;
    @JsonProperty("MaxChargeNum")
    private Integer MaxChargeNum;
    @JsonProperty("LockShape")
    private String LockShape;
    @JsonProperty("LockWeightParams")
    private List<Double> LockWeightParams;
    @JsonProperty("IsAttackCameraLock")
    private Boolean IsAttackCameraLock;
    @JsonProperty("BuffIcon")
    private String BuffIcon;
    @JsonProperty("GlobalValueKey")
    private String GlobalValueKey;
    @JsonProperty("CdTime")
    private Double CdTime;
    @JsonProperty("TriggerID")
    private Integer TriggerID;
    @JsonProperty("DragType")
    private String DragType;
    @JsonProperty("ShowIconArrow")
    private Boolean ShowIconArrow;
    @JsonProperty("ProudSkillGroupId")
    private Integer ProudSkillGroupId;
    @JsonProperty("ForceCanDoSkill")
    private Boolean ForceCanDoSkill;
    @JsonProperty("CostElemType")
    private String CostElemType;
    @JsonProperty("CostElemVal")
    private Double CostElemVal;
    @JsonProperty("IgnoreCDMinusRatio")
    private Boolean IgnoreCDMinusRatio;
    @JsonProperty("IsRanged")
    private Boolean IsRanged;
    @JsonProperty("NeedMonitor")
    private String NeedMonitor;
    @JsonProperty("DefaultLocked")
    private Boolean DefaultLocked;
    @JsonProperty("NeedStore")
    private Boolean NeedStore;
    @JsonProperty("CdSlot")
    private Integer CdSlot;
    @JsonProperty("EnergyMin")
    private Double EnergyMin;
}
