package genhin.json.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)

//@JsonSubTypes({@JsonSubTypes.Type(value = Talent.class)})
public class AvatarTalentExcelConfigData {


    @JsonProperty("TalentId")
    private Integer TalentId;
    @JsonProperty("NameTextMapHash")
    private Long NameTextMapHash;
    @JsonProperty("DescTextMapHash")
    private Long DescTextMapHash;
    @JsonProperty("Icon")
    private String Icon;
    @JsonProperty("MainCostItemId")
    private Integer MainCostItemId;
    @JsonProperty("MainCostItemCount")
    private Integer MainCostItemCount;
    @JsonProperty("OpenConfig")
    private String OpenConfig;
    @JsonProperty("AddProps")
    private List AddProps;
    @JsonProperty("ParamList")
    private List<Double> ParamList;
    @JsonProperty("PrevTalent")
    private Integer PrevTalent;

}
