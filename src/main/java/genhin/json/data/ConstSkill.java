package genhin.json.data;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
//@JsonSubTypes({@JsonSubTypes.Type(value = Talent.class)})
public class ConstSkill implements Talent {


    @JsonProperty("$type")
    public String type;
    private List<Default> avatarAbilitiesDefault = new ArrayList<>();

    @JsonProperty("abilityName")
    private String abilityName;
    @JsonProperty("paramSpecial")
    private String paramSpecial;
    @JsonProperty("paramDelta")
    private String paramDelta;
    @JsonProperty("paramRatio")
    private String paramRatio;
    private List<AvatarSkillExcelConfigData> avatarSkill = new ArrayList<>();
    @JsonProperty("talentParam")
    private String talentParam;
    @JsonProperty("talentType")
    private String talentType;
    @JsonProperty("talentIndex")
    private String talentIndex;
    @JsonProperty("extraLevel")
    private String extraLevel;
    @JsonProperty("skillID")
    private String skillID;
    @JsonProperty("pointDelta")
    private String pointDelta;
    @JsonProperty("cdRatio")
    private String cdRatio;
    @JsonProperty("cdDelta")
    private String cdDelta;
    @JsonProperty("conditionName")
    private String conditionName;


}
