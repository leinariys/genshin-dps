package genhin.json.data;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AvatarTalents implements Map<String, ListTalent> {

    private Map<String, ListTalent> avatarTalents = new HashMap();

    @Override
    public int size() {
        return avatarTalents.size();
    }

    @Override
    public boolean isEmpty() {
        return avatarTalents.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return avatarTalents.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return avatarTalents.containsValue(value);
    }

    @Override
    public ListTalent get(Object key) {
        return avatarTalents.get(key);
    }

    @Override
    public ListTalent put(String key, ListTalent value) {
        return avatarTalents.put(key, value);
    }

    @Override
    public ListTalent remove(Object key) {
        return avatarTalents.remove(key);
    }

    @Override
    public void putAll(Map<? extends String, ? extends ListTalent> m) {
        avatarTalents.putAll(m);
    }

    @Override
    public void clear() {
        avatarTalents.clear();
    }

    @Override
    public Set<String> keySet() {
        return avatarTalents.keySet();
    }

    @Override
    public Collection<ListTalent> values() {
        return avatarTalents.values();
    }

    @Override
    public Set<Entry<String, ListTalent>> entrySet() {
        return avatarTalents.entrySet();
    }

    @Override
    public ListTalent getOrDefault(Object key, ListTalent defaultValue) {
        return avatarTalents.getOrDefault(key, defaultValue);
    }

    @Override
    public void forEach(BiConsumer<? super String, ? super ListTalent> action) {
        avatarTalents.forEach(action);
    }

    @Override
    public void replaceAll(BiFunction<? super String, ? super ListTalent, ? extends ListTalent> function) {
        avatarTalents.replaceAll(function);
    }

    @Override
    public ListTalent putIfAbsent(String key, ListTalent value) {
        return avatarTalents.putIfAbsent(key, value);
    }

    @Override
    public boolean remove(Object key, Object value) {
        return avatarTalents.remove(key,value);
    }

    @Override
    public boolean replace(String key, ListTalent oldValue, ListTalent newValue) {
        return avatarTalents.replace(key, oldValue, newValue);
    }

    @Override
    public ListTalent replace(String key, ListTalent value) {
        return avatarTalents.replace(key, value);
    }

    @Override
    public ListTalent computeIfAbsent(String key, Function<? super String, ? extends ListTalent> mappingFunction) {
        return avatarTalents.computeIfAbsent(key, mappingFunction);
    }

    @Override
    public ListTalent computeIfPresent(String key, BiFunction<? super String, ? super ListTalent, ? extends ListTalent> remappingFunction) {
        return avatarTalents.computeIfPresent(key, remappingFunction);
    }

    @Override
    public ListTalent compute(String key, BiFunction<? super String, ? super ListTalent, ? extends ListTalent> remappingFunction) {
        return avatarTalents.compute(key, remappingFunction);
    }

}




