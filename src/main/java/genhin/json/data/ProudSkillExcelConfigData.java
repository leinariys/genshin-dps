package genhin.json.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ProudSkillExcelConfigData {

    @JsonProperty("ProudSkillId")
    private Integer ProudSkillId;
    @JsonProperty("ProudSkillGroupId")
    private Integer ProudSkillGroupId;
    @JsonProperty("Level")
    private Integer Level;
    @JsonProperty("ProudSkillType")
    private Integer ProudSkillType;
    @JsonProperty("NameTextMapHash")
    private Long NameTextMapHash;
    @JsonProperty("DescTextMapHash")
    private Long DescTextMapHash;
    @JsonProperty("UnlockDescTextMapHash")
    private Long UnlockDescTextMapHash;
    @JsonProperty("Icon")
    private String Icon;
    @JsonProperty("CostItems")
    private List CostItems;
    @JsonProperty("FilterConds")
    private List<String> FilterConds;
    @JsonProperty("BreakLevel")
    private Integer BreakLevel;
    @JsonProperty("ParamDescList")
    private List<Long> ParamDescList;
    @JsonProperty("LifeEffectParams")
    private List<String> LifeEffectParams;
    @JsonProperty("OpenConfig")
    private String OpenConfig;
    @JsonProperty("AddProps")
    private List AddProps;
    @JsonProperty("ParamList")
    private List<Double> ParamList;
    @JsonProperty("LifeEffectType")
    private String LifeEffectType;
    @JsonProperty("CoinCost")
    private Integer CoinCost;
    @JsonProperty("EffectiveForTeam")
    private Integer EffectiveForTeam;
    @JsonProperty("NBDKELEBHGB")
    private Boolean NBDKELEBHGB;

}
