package genhin.json.data;


import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = ConstSkill.class)

@JsonSubTypes({
        @JsonSubTypes.Type(value = ConstSkill.class),
        @JsonSubTypes.Type(value = AvatarTalentExcelConfigData.class)
})

//@JsonTypeInfo(use = JsonTypeInfo.Id.NONE , include = JsonTypeInfo.As.PROPERTY, property = "$type", defaultImpl = ConstSkill.class)
/*
@JsonSubTypes({
        @JsonSubTypes.Type(value = ConstSkill.class, name = "$type"),
        @JsonSubTypes.Type(value = AvatarTalentExcelConfigData.class, name = "$type")
})
*/
//@JsonDeserialize(using = ConstSkill.class)
public interface Talent {

}

