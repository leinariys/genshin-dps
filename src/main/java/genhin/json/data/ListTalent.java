package genhin.json.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.*;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ListTalent implements List<Talent> {

    private List<Talent> talents = new ArrayList<Talent>();

    private List<AvatarTalentExcelConfigData> avatarTalentExcelConfigData = new ArrayList<AvatarTalentExcelConfigData>();

    private List<ProudSkillExcelConfigData> proudSkillExcelConfigData = new ArrayList<ProudSkillExcelConfigData>();

    public boolean addAvatarTalentExcelConfigData(AvatarTalentExcelConfigData talent) {
        return avatarTalentExcelConfigData.add(talent);
    }

    public AvatarTalentExcelConfigData getAvatarTalentExcelConfigData(int index) {
        return avatarTalentExcelConfigData.get(index);
    }

    public Iterator<AvatarTalentExcelConfigData> iteratorAvatarTalentExcelConfigData() {
        return avatarTalentExcelConfigData.iterator();
    }

    public boolean addProudSkillExcelConfigData(ProudSkillExcelConfigData talent) {
        return proudSkillExcelConfigData.add(talent);
    }

    public ProudSkillExcelConfigData getProudSkillExcelConfigData(int index) {
        return proudSkillExcelConfigData.get(index);
    }

    public Iterator<ProudSkillExcelConfigData> iteratorProudSkillExcelConfigData() {
        return proudSkillExcelConfigData.iterator();
    }

    @Override
    public int size() {
        return talents.size();
    }

    @Override
    public boolean isEmpty() {
        return talents.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return talents.contains(o);
    }

    @Override
    public Iterator<Talent> iterator() {
        return talents.iterator();
    }

    @Override
    public Object[] toArray() {
        return talents.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return talents.toArray(a);
    }

    @Override
    public boolean add(Talent talent) {
        return talents.add(talent);
    }

    @Override
    public boolean remove(Object o) {
        return talents.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return talents.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends Talent> c) {
        return talents.addAll(c);
    }

    @Override
    public boolean addAll(int index, Collection<? extends Talent> c) {
        return talents.addAll(index, c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return talents.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return talents.retainAll(c);
    }

    @Override
    public void clear() {
        talents.clear();
    }

    @Override
    public Talent get(int index) {
        return talents.get(index);
    }

    @Override
    public Talent set(int index, Talent element) {
        return talents.set(index, element);
    }

    @Override
    public void add(int index, Talent element) {
        talents.add(index, element);
    }

    @Override
    public Talent remove(int index) {
        return talents.remove(index);
    }

    @Override
    public int indexOf(Object o) {
        return talents.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return talents.lastIndexOf(o);
    }

    @Override
    public ListIterator<Talent> listIterator() {
        return talents.listIterator();
    }

    @Override
    public ListIterator<Talent> listIterator(int index) {
        return talents.listIterator(index);
    }

    @Override
    public List<Talent> subList(int fromIndex, int toIndex) {
        return talents.subList(fromIndex, toIndex);
    }
}
