package genhin.json.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;



@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Default {

    @JsonProperty("abilitySpecials")
    private Object abilitySpecials;
    @JsonProperty("abilityName")
    private String abilityName;
    @JsonProperty("onAbilityStart")
    private List onAbilityStart;
    @JsonProperty("abilityMixins")
    private List abilityMixins;
    @JsonProperty("modifiers")
    private Object modifiers;
    @JsonProperty("$type")
    private String type;
    @JsonProperty("onAdded")
    private List onAdded;
    @JsonProperty("onKill")
    private List onKill;
    @JsonProperty("isDynamicAbility")
    private Boolean isDynamicAbility;
    @JsonProperty("onFieldEnter")
    private List onFieldEnter;
    @JsonProperty("onFieldExit")
    private List onFieldExit;
    @JsonProperty("onRemoved")
    private List onRemoved;
    @JsonProperty("onAvatarIn")
    private List onAvatarIn;
    @JsonProperty("onAvatarOut")
    private List onAvatarOut;
}
