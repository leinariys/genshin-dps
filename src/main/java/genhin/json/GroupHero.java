package genhin.json;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;


/**
 * Gати персов
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GroupHero extends NadaTree {

    public GroupHero() {
    }

    public GroupHero(String name) {
        this.name = name;
    }

    private List<Hero> heroes = new ArrayList<>();

    @JsonIgnore
    public void setCharact(Hero... charact) {
        for (Hero item : charact) {
            this.heroes.add(item);
        }
    }

    @Override
    public String toString() {
        return String.format("%s [%s]", name, heroes.size()); // getClass().getName() + "@" + Integer.toHexString(hashCode());
    }
}
