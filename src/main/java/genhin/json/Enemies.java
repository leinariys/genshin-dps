package genhin.json;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Enemies {

    private String name;
    private int lvl;

    /**
     * https://genshin-impact.fandom.com/wiki/Damage#Base_Enemy_Resistances
     */

    private List<ValueOption> options = new ArrayList<ValueOption>();

    public Enemies() {
    }

    public Enemies(String name, int lvl) {
        this.name = name;
        this.lvl = lvl;
    }


    /**
     * Получить сопративление противника
     *
     * @param elementalRes
     * @return
     */
    @JsonIgnore
    public double getRes(TypeElement elementalRes) {
        Optional<ValueOption> resEnemies = options.stream()
                .filter(f -> elementalRes.equals(f.getDamageTypeElement()))
                .findFirst();
        return resEnemies.orElse(new ValueOption()).getValueMain();
    }
}
