/**
 * Sample Skeleton for 'itemOption.fxml' Controller Class
 */

package genhin.controller;

import genhin.json.*;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.stream.Stream;

public class ItemOption {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="title"
    private TextField title; // Value injected by FXMLLoader

    @FXML // fx:id="typeGroup"
    private MenuButton typeGroup; // Value injected by FXMLLoader

    @FXML // fx:id="typeOption"
    private MenuButton typeOption; // Value injected by FXMLLoader

    @FXML // fx:id="typeAbility"
    private MenuButton typeAbility; // Value injected by FXMLLoader

    @FXML // fx:id="damageTypeElement"
    private MenuButton damageTypeElement; // Value injected by FXMLLoader

    @FXML // fx:id="typeElementStatus"
    private MenuButton typeElementStatus; // Value injected by FXMLLoader

    @FXML // fx:id="isAffectsGroup"
    private CheckBox isAffectsGroup; // Value injected by FXMLLoader

    @FXML // fx:id="index"
    private Spinner<Integer> index; // Value injected by FXMLLoader

    @FXML // fx:id="valueMain"
    private Spinner<Double> valueMain; // Value injected by FXMLLoader

    @FXML // fx:id="valueMult2"
    private Spinner<Double> valueMult2; // Value injected by FXMLLoader

    @FXML // fx:id="valueAdd"
    private Spinner<Double> valueAdd; // Value injected by FXMLLoader

    @FXML // fx:id="teak"
    private Spinner<Double> teak; // Value injected by FXMLLoader

    @FXML // fx:id="timeAniMs"
    private Spinner<Double> timeAniMs; // Value injected by FXMLLoader

    @FXML // fx:id="timeRollbackMs"
    private Spinner<Double> timeRollbackMs; // Value injected by FXMLLoader

    @FXML
        // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert title != null : "fx:id=\"title\" was not injected: check your FXML file 'itemOption.fxml'.";
        assert typeGroup != null : "fx:id=\"typeGroup\" was not injected: check your FXML file 'itemOption.fxml'.";
        assert typeOption != null : "fx:id=\"typeOption\" was not injected: check your FXML file 'itemOption.fxml'.";
        assert typeAbility != null : "fx:id=\"typeAbility\" was not injected: check your FXML file 'itemOption.fxml'.";
        assert damageTypeElement != null : "fx:id=\"damageTypeElement\" was not injected: check your FXML file 'itemOption.fxml'.";
        assert typeElementStatus != null : "fx:id=\"typeElementStatus\" was not injected: check your FXML file 'itemOption.fxml'.";
        assert isAffectsGroup != null : "fx:id=\"isAffectsGroup\" was not injected: check your FXML file 'itemOption.fxml'.";
        assert index != null : "fx:id=\"index\" was not injected: check your FXML file 'itemOption.fxml'.";
        assert valueMain != null : "fx:id=\"valueMain\" was not injected: check your FXML file 'itemOption.fxml'.";
        assert valueMult2 != null : "fx:id=\"valueMult2\" was not injected: check your FXML file 'itemOption.fxml'.";
        assert valueAdd != null : "fx:id=\"valueAdd\" was not injected: check your FXML file 'itemOption.fxml'.";
        assert teak != null : "fx:id=\"teak\" was not injected: check your FXML file 'itemOption.fxml'.";
        assert timeAniMs != null : "fx:id=\"timeAniMs\" was not injected: check your FXML file 'itemOption.fxml'.";
        assert timeRollbackMs != null : "fx:id=\"timeRollbackMs\" was not injected: check your FXML file 'itemOption.fxml'.";
    }


    private <T> void setMenuButton(MenuButton menu, Stream<T> stream, ValueOption valueOption) {
        menu.getItems().clear();

        stream.forEach(item -> {

            MenuItem itemM = new MenuItem(item.toString());
            menu.getItems().add(itemM);
            itemM.setOnAction(it -> {
                menu.setText(itemM.getText());

                if (menu.equals(typeGroup)) {
                    valueOption.setTypeGroup(TypeGroup.valueOf(typeGroup.getText()));
                }
                if (menu.equals(typeOption)) {
                    valueOption.setTypeOption(TypeOption.valueOf(typeOption.getText()));
                }
                if (menu.equals(typeAbility)) {
                    valueOption.setTypeAbility(TypeAbility.valueOf(typeAbility.getText()));
                }
                if (menu.equals(damageTypeElement)) {
                    valueOption.setDamageTypeElement(TypeElement.valueOf(damageTypeElement.getText()));
                }
                if (menu.equals(typeElementStatus)) {
                    valueOption.setTypeElementStatus(TypeElementStatus.valueOf(typeElementStatus.getText()));
                }

            });

            menu.setText(item.toString());
        });
    }


    public void init(ValueOption item) {
        System.out.println("Отобразить опцию:" + item.getName());

        title.setPromptText("Название");
        title.setText(item.getName());
        title.setOnKeyReleased(e -> {
            item.setName(title.getText());
        });

        setMenuButton(typeGroup, Arrays.stream(TypeGroup.values()), item);
        typeGroup.setText(item.getTypeGroup().toString());
        setMenuButton(typeOption, Arrays.stream(TypeOption.values()), item);
        typeOption.setText(item.getTypeOption().toString());
        setMenuButton(typeAbility, Arrays.stream(TypeAbility.values()), item);
        typeAbility.setText(item.getTypeAbility().toString());
        setMenuButton(damageTypeElement, Arrays.stream(TypeElement.values()), item);
        damageTypeElement.setText(item.getDamageTypeElement().toString());
        setMenuButton(typeElementStatus, Arrays.stream(TypeElementStatus.values()), item);
        typeElementStatus.setText(item.getTypeElementStatus().toString());

        isAffectsGroup.setSelected(item.isAffectsGroup());
        isAffectsGroup.setOnAction(e -> {
            item.setAffectsGroup(isAffectsGroup.isSelected());
        });


        index.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 1000, item.getIndex(), 1));
        index.setOnMouseClicked(e -> {
            item.setIndex(index.getValue());
        });
        valueMain.setValueFactory(new SpinnerValueFactory.DoubleSpinnerValueFactory(1, 1000, item.getValueMain(), 0.1d));
        valueMain.setOnMouseClicked(e -> {
            item.setValueMain(valueMain.getValue());
        });
        valueMult2.setValueFactory(new SpinnerValueFactory.DoubleSpinnerValueFactory(0, 1000, item.getValueMult2(), 0.1d));
        valueMult2.setOnMouseClicked(e -> {
            item.setValueMult2(valueMult2.getValue());
        });
        valueAdd.setValueFactory(new SpinnerValueFactory.DoubleSpinnerValueFactory(0, 1000, item.getValueAdd(), 0.1d));
        valueAdd.setOnMouseClicked(e -> {
            item.setValueAdd(valueAdd.getValue());
        });
        teak.setValueFactory(new SpinnerValueFactory.DoubleSpinnerValueFactory(1, 1000, item.getTeak(), 0.1d));
        teak.setOnMouseClicked(e -> {
            item.setTeak(teak.getValue());
        });
        timeAniMs.setValueFactory(new SpinnerValueFactory.DoubleSpinnerValueFactory(0, 1000, item.getTimeAniMs(), 0.1d));
        timeAniMs.setOnMouseClicked(e -> {
            item.setTimeAniMs(timeAniMs.getValue());
        });
        timeRollbackMs.setValueFactory(new SpinnerValueFactory.DoubleSpinnerValueFactory(0, 1000, item.getTimeRollbackMs(), 0.1d));
        timeRollbackMs.setOnMouseClicked(e -> {
            item.setTimeRollbackMs(timeRollbackMs.getValue());
        });


    }
}
