/**
 * Sample Skeleton for 'topMenuBar.fxml' Controller Class
 */

package genhin.controller;

import genhin.app.LogicApp;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;

import java.net.URL;
import java.util.ResourceBundle;

public class TopMenuBar {


    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML
    void menuBarAbout(ActionEvent event) {
        LogicApp.menuBarAbout();
    }

    @FXML
    void menuBarVersion(ActionEvent event) {
        LogicApp.menuBarVersion();
    }
    @FXML
    void menuBarAuthor(ActionEvent event) {
        LogicApp.menuBarAuthor();
    }
    @FXML
    void menuBarContact(ActionEvent event) {
        LogicApp.menuBarContact();
    }
    @FXML
    void menuLog(ActionEvent event) {
        LogicApp.menuLog();
    }

    @FXML
    void menuBarClose(ActionEvent event) {
        System.out.println(event.toString());
    }

    @FXML
    void menuBarCopy(ActionEvent event) {
        System.out.println(event.toString());
    }

    @FXML
    void menuBarCut(ActionEvent event) {
        System.out.println(event.toString());
    }

    @FXML
    void menuBarDelete(ActionEvent event) {
        System.out.println(event.toString());
    }

    @FXML
    void menuBarOpenRecent(ActionEvent event) {
        System.out.println(event.toString());
    }

    @FXML
    void menuBarPaste(ActionEvent event) {
        System.out.println(event.toString());
    }

    @FXML
    void menuBarPreferences(ActionEvent event) {
        System.out.println(event.toString());
    }

    @FXML
    void menuBarQuit(ActionEvent event) {
        System.out.println(event.toString());
    }

    @FXML
    void menuBarRedo(ActionEvent event) {
        System.out.println(event.toString());
    }

    @FXML
    void menuBarRevert(ActionEvent event) {
        System.out.println(event.toString());
    }

    @FXML
    void menuBarSaveAs(ActionEvent event) {
        System.out.println(event.toString());
    }

    @FXML
    void menuBarSelectAll(ActionEvent event) {
        System.out.println(event.toString());
    }

    @FXML
    void menuBarUndo(ActionEvent event) {
        System.out.println(event.toString());
    }

    @FXML
    void menuBarUnselectAll(ActionEvent event) {
        System.out.println(event.toString());
    }

    @FXML
    void menuBarNew(ActionEvent event) {
        LogicApp.menuBarNewEvent();
    }

    @FXML
    void menuBarOpen(ActionEvent event) {
        LogicApp.menuBarOpenEvent();
    }

    @FXML
    void menuBarSave(ActionEvent event) {
        LogicApp.menuBarSaveEvent();
    }


    @FXML
        // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        System.out.println("location:" + location.toString());
    }

}




