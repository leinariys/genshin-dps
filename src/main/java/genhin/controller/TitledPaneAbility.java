package genhin.controller;

import genhin.json.GroupHero;
import genhin.json.TypeAbility;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;

public class TitledPaneAbility {


    private final Node rootIcon = new ImageView(
            new Image(getClass().getResourceAsStream("/img/avatar/UI_AvatarIcon_Keqing.png"))
    );
    @FXML // fx:id="titledPane"
    private TitledPane titleGroup; // Value injected by FXMLLoader
    @FXML // fx:id="menuAbility"
    private MenuButton menuAbility; // Value injected by FXMLLoader
    @FXML // fx:id="listAbility"
    private HBox hBoxAbility; // Value injected by FXMLLoader

    @FXML
    void clickedItemAbility(MouseEvent event) {
        System.out.println(event.toString());
    }

    @FXML
    void newAbility(ActionEvent event) {
        System.out.println(event.toString());

        hBoxAbility.getChildren().add(new Button(menuAbility.getText()));
    }

    @FXML
        // This method is called by the FXMLLoader when initialization is complete
    void initialize() {

        assert titleGroup != null : "fx:id=\"titledPane\" was not injected: check your FXML file 'topMenuBar.fxml'.";
        assert menuAbility != null : "fx:id=\"selectAbility\" was not injected: check your FXML file 'topMenuBar.fxml'.";
        assert hBoxAbility != null : "fx:id=\"hBoxAbility\" was not injected: check your FXML file 'topMenuBar.fxml'.";

        System.out.println("titleGroup:" + titleGroup.getText());
        System.out.println("selectAbilityText:" + menuAbility.getText());
        System.out.println("hBoxAbility:" + hBoxAbility.getChildren().size());


        setMenuAllAbility();


    }

    private void setMenuAllAbility() {
        menuAbility.getItems().clear();
        for (TypeAbility item : TypeAbility.values()) {
            MenuItem itemM = new MenuItem(item.name());
            menuAbility.getItems().add(itemM);
            itemM.setOnAction(it -> {
                menuAbility.setText(itemM.getText());
            });
        }
        menuAbility.setText(TypeAbility.ATTACK_AIM.name());
    }

    private void setTitleGroup(String titleGroup) {
        this.titleGroup.setText(titleGroup);
    }


    public void init(GroupHero itemGroupHero) {

    }
}
