/**
 * Sample Skeleton for 'mainWindow.fxml' Controller Class
 */


package genhin.controller;

import genhin.app.LogicApp;
import genhin.json.GroupHero;
import genhin.json.Hero;
import genhin.json.NadaTree;
import genhin.json.ValueOption;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


@Slf4j
public class MainWindow {

    private final Node rootIcon = new ImageView(
            new Image(getClass().getResourceAsStream("/img/avatar/UI_AvatarIcon_Keqing.png"))
    );


    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;
    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;
    private NadaTree rootTree = new NadaTree("Groups Hero");
    @FXML // fx:id="treeViewGroup"
    private TreeView<NadaTree> treeViewGroup; // Value injected by FXMLLoader
    private TreeItem<NadaTree> treeGroup = new TreeItem<NadaTree>(rootTree);

    @FXML // fx:id="listViewValueOption"
    private ListView<FlowPane> listViewValueOption; // Value injected by FXMLLoader
    private ObservableList<FlowPane> listValueOption = FXCollections.observableArrayList();

    @FXML // fx:id="accordionGroup"
    private Accordion accordionGroup; // Value injected by FXMLLoader

    @FXML
    public void clickedItemHero(MouseEvent event) {
        LogicApp.updateUIlist(treeViewGroup.getSelectionModel().getSelectedItem());
    }


    @FXML
    void initialize() {
        assert treeViewGroup != null : "fx:id=\"treeViewGroup\" was not injected: check your FXML file 'mainWindow.fxml'.";
        assert listViewValueOption != null : "fx:id=\"listViewValueOption\" was not injected: check your FXML file 'mainWindow.fxml'.";
        assert accordionGroup != null : "fx:id=\"accordion\" was not injected: check your FXML file 'mainWindow.fxml'.";

        treeViewGroup.setRoot(treeGroup);
        treeGroup.setExpanded(true);
        listViewValueOption.setItems(listValueOption);

        LogicApp.setTreeGroup(treeGroup);
        LogicApp.setListValueOption(listValueOption);
        LogicApp.setAccordionGroup(accordionGroup);

        treeContextMenu();
        listContextMenu();
    }


    private void treeContextMenu() {
        MenuItem entry1 = new MenuItem("New Group");
        entry1.setOnAction(event -> {
            System.out.println("new Group ContextMenu:" + event);

            GroupHero groupHero = LogicApp.addNewGroupHero(
                    LogicApp.showDialogEditor(
                            "Новая/Edit Группа Dialog",
                            "Задайте имя группы",
                            "Имя:",
                            "Новая Группа"));
            TreeItem<NadaTree> group = new TreeItem<NadaTree>(groupHero);
            treeGroup.getChildren().add(group);

            accordionGroup.getPanes().add(LogicApp.newItemPaneAbility(groupHero));

            TreeItem<NadaTree> selectedItem = treeViewGroup.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                NadaTree nadaTree = selectedItem.getValue();
            }
        });

        MenuItem entry2 = new MenuItem("New Hero");
        entry2.setOnAction(event -> {
            System.out.println("new Hero ContextMenu:" + event);

            TreeItem<NadaTree> selectedItem = treeViewGroup.getSelectionModel().getSelectedItem();
            if (selectedItem != null && selectedItem.getParent() != null) {

                String name = LogicApp.showDialogEditor("Новая/Edit Группа Dialog", "Задайте имя группы", "Имя:", "Новый Герой");

                if (isTreeItemGroup(selectedItem)) {//строка Группа

                    Hero newHero = LogicApp.addNewHero(name, selectedItem.getValue().getId());
                    TreeItem<NadaTree> newTreeHero = new TreeItem<NadaTree>(newHero);
                    selectedItem.getChildren().add(newTreeHero);
                    selectedItem.setExpanded(true);
                } else {//строка Герои

                    Hero newHero = LogicApp.addNewHero(name, selectedItem.getParent().getValue().getId());
                    TreeItem<NadaTree> newTreeHero = new TreeItem<NadaTree>(newHero);
                    selectedItem.getParent().getChildren().add(newTreeHero);
                    selectedItem.getParent().setExpanded(true);
                }
            }
            treeViewGroup.refresh();
        });

        MenuItem entry3 = new MenuItem("Editor");
        entry3.setOnAction(event -> {
            System.out.println("EditorTreeContextMenu:" + event);
            TreeItem<NadaTree> selectedItem = treeViewGroup.getSelectionModel().getSelectedItem();
            if (selectedItem != null && selectedItem.getParent() != null) {
                NadaTree nadaTreeName = selectedItem.getValue();
                nadaTreeName.setName(LogicApp.showDialogEditor("Новая/Edit Группа Dialog", "Задайте имя ", "Имя:", nadaTreeName.getName()));

                if (isTreeItemGroup(selectedItem)) {//строка Группа
                } else {//строка Герои
                    Hero nadaTreeLvL = (Hero) selectedItem.getValue();
                    String lvl = LogicApp.showDialogEditor("Новая/Edit Группа Dialog", "Задайте LvL Героя", "LvL:", String.valueOf(nadaTreeLvL.getLvl()));
                    nadaTreeLvL.setLvl(Integer.parseInt(lvl));
                }
            }
            treeViewGroup.refresh();
        });

        MenuItem entry4 = new MenuItem("Select");
        entry4.setOnAction(event -> {
            System.out.println("deleteTreeContextMenu:" + event);
            TreeItem<NadaTree> selectedItem = treeViewGroup.getSelectionModel().getSelectedItem();
            if (selectedItem != null && selectedItem.getParent() != null) {
                if (isTreeItemGroup(selectedItem)) {//Группа
                } else {//Герои
                    GroupHero GroupHero = (GroupHero) selectedItem.getParent().getValue();
                    GroupHero.getHeroes().stream().forEach(item -> item.setSelect(false));
                    Hero nadaTreeLvL = (Hero) selectedItem.getValue();
                    nadaTreeLvL.setSelect(true);
                }
            }
            treeViewGroup.refresh();
        });

        MenuItem entry5 = new MenuItem("Delete");
        entry5.setOnAction(event -> {
            System.out.println("deleteTreeContextMenu:" + event);
            TreeItem<NadaTree> selectedItem = treeViewGroup.getSelectionModel().getSelectedItem();
            if (selectedItem != null && selectedItem.getParent() != null) {

                if (isTreeItemGroup(selectedItem)) {//Группа
                    treeGroup.getChildren().remove(selectedItem);
                } else {//Герои
                    treeGroup.getChildren().stream().forEach(f -> f.getChildren().remove(selectedItem));
                }

            }
            treeViewGroup.refresh();
        });

        treeViewGroup.setContextMenu(new ContextMenu(entry1, entry2, entry3, entry4, entry5));
    }

    private boolean isTreeItemGroup(TreeItem<NadaTree> selectedItem) {
        return selectedItem.getParent().equals(treeGroup);
    }


    private void listContextMenu() {
        MenuItem entry1 = new MenuItem("New");
        entry1.setOnAction(event -> {
            System.out.println("newItemListOption:" + event);

            TreeItem<NadaTree> selectedItem = treeViewGroup.getSelectionModel().getSelectedItem();
            if (selectedItem != null && selectedItem.getParent() != null) {

                if (!isTreeItemGroup(selectedItem)) {//Герои
                    ValueOption newValueOption = LogicApp.addNewValueOption(selectedItem.getParent().getValue().getId(), selectedItem.getValue().getId());
                    listValueOption.add(LogicApp.newItemOption(newValueOption));
                }

            }

        });
        MenuItem entry2 = new MenuItem("Delete");
        entry2.setOnAction(event -> {
            System.out.println("deleteItemListOption:" + event);
            FlowPane selectedItem = listViewValueOption.getSelectionModel().getSelectedItem();

            listValueOption.remove(selectedItem);


        });
        listViewValueOption.setContextMenu(new ContextMenu(entry1, entry2));
    }




}
