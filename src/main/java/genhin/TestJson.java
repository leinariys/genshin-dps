package genhin;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import genhin.json.data.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

public class TestJson {

    private static ObjectMapper mapper = new ObjectMapper();
    private static Map<String, List<AvatarAbilities>> avatarAbilities = new HashMap<>();
    private static Map<String, AvatarTalents> avatarTalents = new HashMap<>();
    private static List<AvatarSkillExcelConfigData> avatarSkillExcelConfigData;
    private static List<AvatarTalentExcelConfigData> avatarTalentExcelConfigData;
    private static List<ProudSkillExcelConfigData> proudSkillExcelConfigData;
    private static Map<String, String> mapTextMapRU;


    public static void main(String[] args) throws IOException {


        testGrup();


        //  testInfo();

    }

    private static void testGrup() throws IOException {


        File filenamesAvatarAbilities = new File("src/main/resources/json/AvatarAbilities/");
        for (File fileEntry : filenamesAvatarAbilities.listFiles()) {
            if (fileEntry.isFile()) {
                System.out.println(fileEntry.getName());
                String[] split = fileEntry.getName().split("_");
                avatarAbilities.put(split[split.length - 1], avatarAbilities(fileEntry.getPath()));
            }
        }
        File filenamesAvatarTalents = new File("src/main/resources/json/AvatarTalents/");
        for (File fileEntry : filenamesAvatarTalents.listFiles()) {
            if (fileEntry.isFile()) {
                System.out.println(fileEntry.getName());
                String[] split = fileEntry.getName().split("_");
                avatarTalents.put(split[split.length - 1], avatarTalents(fileEntry.getPath()));
            }
        }
        avatarSkillExcelConfigData = avatarSkillExcelConfigData("src/main/resources/json/AvatarSkillExcelConfigData.json");
        avatarTalentExcelConfigData = avatarTalentExcelConfigData("src/main/resources/json/AvatarTalentExcelConfigData.json");
        proudSkillExcelConfigData = proudSkillExcelConfigData("src/main/resources/json/ProudSkillExcelConfigData.json");
        mapTextMapRU = textMapRU("src/main/resources/json/TextMapRU.json");

        intelligence();

        System.out.println(avatarTalents);

        avatarTalents.forEach((keys, item) -> {
            try {
                mapper.writeValue(new File("src/main/resources/json/newTalant/" + keys), item);
            } catch (IOException e) {
                e.printStackTrace();
            }


        });


    }

    private static void intelligence() {

        avatarTalents.entrySet().stream().forEach((fileTalHero) -> {
            //key = "Ningguang.json" value = {AvatarTalents@1461}  size = 12

            fileTalHero.getValue().entrySet().stream().forEach((avarTal) -> {
                //avarTal   key = "Ningguang_Constellation_5" value = {ListTalent@1413}  size = 1
                avatarTalentExcelConfigData.stream().forEach(avatarTalentExcelConfigData1 -> {
                    //AvatarTalentExcelConfigData
                    if (avarTal.getKey().equals(avatarTalentExcelConfigData1.getOpenConfig())) {
                        System.out.println(avarTal.getKey() + "___" + avatarTalentExcelConfigData1.getOpenConfig());
                        avarTal.getValue().addAvatarTalentExcelConfigData(avatarTalentExcelConfigData1);
                    }
                });
                proudSkillExcelConfigData.stream().forEach(proudSkillExcelConfigData -> {
                    if (avarTal.getKey().equals(proudSkillExcelConfigData.getOpenConfig())) {
                        System.out.println(avarTal.getKey() + "___" + proudSkillExcelConfigData.getOpenConfig());
                        avarTal.getValue().addProudSkillExcelConfigData(proudSkillExcelConfigData);
                    }
                });


                fileTalHero.getValue().values().stream().forEach(itemTalent -> {
                    itemTalent.getTalents().stream().forEach((talent) -> {
                        // talents =  ConstSkill
                        if (((ConstSkill) talent).getAbilityName() != null) {

                            avatarAbilities.get(fileTalHero.getKey()).forEach((abili) -> {
                                //AvatarAbilities
                                if (abili.getDefault().getAbilityName() != null) {
                                    if (((ConstSkill) talent).getAbilityName().equals(abili.getDefault().getAbilityName())) {
                                        ((ConstSkill) talent).getAvatarAbilitiesDefault().add(abili.getDefault());

                                    }
                                }
                            });

                            avatarSkillExcelConfigData.forEach((avatarSkill) -> {
                                if (avatarSkill.getAbilityName() != null) {
                                    if (((ConstSkill) talent).getAbilityName().equals(avatarSkill.getAbilityName())) {
                                        ((ConstSkill) talent).getAvatarSkill().add(avatarSkill);


                                        avatarSkill.setNameTextMapHashString(mapTextMapRU.get(avatarSkill.getNameTextMapHash().toString()));
                                        avatarSkill.setDescTextMapHashString(mapTextMapRU.get(avatarSkill.getDescTextMapHash().toString()));


                                    }
                                }
                            });
                        }
                    });
                });
            });
        });


    }

    private static void testInfo() throws IOException {

        viewObj("src/main/resources/json/TextMapRU.json");


        if (false) {
            File filenamesAvatarAbilities = new File("src/main/resources/json/AvatarAbilities/");
            for (File fileEntry : filenamesAvatarAbilities.listFiles()) {
                if (fileEntry.isFile()) {
                    System.out.println(fileEntry.getName());
                    viewMap(fileEntry.getPath());
                }
            }
            File filenamesAvatarTalents = new File("src/main/resources/json/AvatarTalents/");
            for (File fileEntry : filenamesAvatarTalents.listFiles()) {
                if (fileEntry.isFile()) {
                    System.out.println(fileEntry.getName());
                    viewObj(fileEntry.getPath());
                }
            }

            viewMap("src/main/resources/json/AvatarSkillExcelConfigData.json");
            viewMap("src/main/resources/json/AvatarTalentExcelConfigData.json");
            viewMap("src/main/resources/json/ProudSkillExcelConfigData.json");

        } else {
            File filenamesAvatarAbilities = new File("src/main/resources/json/AvatarAbilities/");
            for (File fileEntry : filenamesAvatarAbilities.listFiles()) {
                if (fileEntry.isFile()) {
                    System.out.println(fileEntry.getName());
                    avatarAbilities(fileEntry.getPath());
                }
            }
            File filenamesAvatarTalents = new File("src/main/resources/json/AvatarTalents/");
            for (File fileEntry : filenamesAvatarTalents.listFiles()) {
                if (fileEntry.isFile()) {
                    System.out.println(fileEntry.getName());
                    avatarTalents(fileEntry.getPath());
                }
            }
            avatarSkillExcelConfigData("src/main/resources/json/AvatarSkillExcelConfigData.json");
            avatarTalentExcelConfigData("src/main/resources/json/AvatarTalentExcelConfigData.json");
            proudSkillExcelConfigData("src/main/resources/json/ProudSkillExcelConfigData.json");
            textMapRU("src/main/resources/json/TextMapRU.json");
        }
    }


    private static void viewObj(String filename) throws IOException {
        System.out.println("\nfilenames:" + filename);
        List<String> arrayList = new ArrayList<>();
        Map<String, String> map = mapper.readValue(Paths.get(filename).toFile(), Map.class);
        for (Map.Entry<String, String> entry : map.entrySet()) {

            if (!arrayList.contains(entry.getKey())) {
                //System.out.println("@JsonProperty(\""+k+"\") private"+v.getClass()+" "+k+";");
                arrayList.add(entry.getKey());
            }

        }
        System.out.println("\narrayList:" + arrayList.size());
    }

    private static void viewMap(String filename) throws IOException {
        System.out.println("\nfilenames:" + filename);
        List<String> arrayList = new ArrayList<>();
        List<Map> map = mapper.readValue(Paths.get(filename).toFile(), new TypeReference<List<Map>>() {
        });
        for (Map entry : map) {
            entry.forEach((k, v) ->
            {
                if (!arrayList.contains(k)) {
                    System.out.println("@JsonProperty(\"" + k + "\") private " + v.getClass() + " " + k + ";");
                    arrayList.add((String) k);
                }
            });
        }
    }


    private static List<AvatarAbilities> avatarAbilities(String filename) throws IOException {
        System.out.println("\nfilenames:" + filename);
        List<String> arrayList = new ArrayList<>();
        List<AvatarAbilities> avatarSkillExcelConfigData = mapper.readValue(Paths.get(filename).toFile(), new TypeReference<List<AvatarAbilities>>() {
        });
        System.out.println("length:" + avatarSkillExcelConfigData.size());
        for (AvatarAbilities data : avatarSkillExcelConfigData) {

            if (!arrayList.contains(data.getDefault().getAbilityName())) {
                arrayList.add(data.getDefault().getAbilityName());
                System.out.println(data.getDefault().getAbilityName());

            }
        }
        return avatarSkillExcelConfigData;
    }

    private static AvatarTalents avatarTalents(String filename) throws IOException {
        System.out.println("\nfilenames:" + filename);
        List<String> arrayList = new ArrayList<>();
        //mapper.registerModule(new ConstSkill());
        AvatarTalents avatarTalents = mapper.readValue(Paths.get(filename).toFile(), AvatarTalents.class);
 /*       System.out.println("length:" + avatarTalents.size());
        for (Object data : avatarTalents.entrySet()) {

            if (!arrayList.contains(data.getAbilityName())) {
                arrayList.add(data.getAbilityName());
                System.out.println(data.getAbilityName());

            }

        }
        */
        return avatarTalents;
    }

    private static List<AvatarSkillExcelConfigData> avatarSkillExcelConfigData(String filename) throws IOException {
        System.out.println("\nfilenames:" + filename);
        List<String> arrayList = new ArrayList<>();
        List<AvatarSkillExcelConfigData> avatarSkillExcelConfigData = mapper.readValue(Paths.get(filename).toFile(), new TypeReference<List<AvatarSkillExcelConfigData>>() {
        });
        System.out.println("length:" + avatarSkillExcelConfigData.size());
        for (AvatarSkillExcelConfigData data : avatarSkillExcelConfigData) {

            if (!arrayList.contains(data.getAbilityName())) {
                arrayList.add(data.getAbilityName());
                System.out.println(data.getAbilityName());

            }
        }
        return avatarSkillExcelConfigData;
    }

    private static List<AvatarTalentExcelConfigData> avatarTalentExcelConfigData(String filename) throws IOException {
        System.out.println("\nfilenames:" + filename);
        List<String> arrayList = new ArrayList<>();
        List<AvatarTalentExcelConfigData> avatarSkillExcelConfigData = mapper.readValue(Paths.get(filename).toFile(), new TypeReference<List<AvatarTalentExcelConfigData>>() {
        });
        System.out.println("length:" + avatarSkillExcelConfigData.size());
        for (AvatarTalentExcelConfigData data : avatarSkillExcelConfigData) {

            if (!arrayList.contains(data.getOpenConfig())) {
                arrayList.add(data.getOpenConfig());
                System.out.println(data.getOpenConfig());

            }
        }
        return avatarSkillExcelConfigData;
    }

    private static List<ProudSkillExcelConfigData> proudSkillExcelConfigData(String filename) throws IOException {
        System.out.println("\nfilenames:" + filename);
        List<String> arrayList = new ArrayList<>();
        ProudSkillExcelConfigData[] avatarSkillExcelConfigData = mapper.readValue(Paths.get(filename).toFile(), ProudSkillExcelConfigData[].class);
        System.out.println("length:" + avatarSkillExcelConfigData.length);
        for (ProudSkillExcelConfigData data : avatarSkillExcelConfigData) {

            if (!arrayList.contains(data.getOpenConfig())) {
                arrayList.add(data.getOpenConfig());
                System.out.println(data.getOpenConfig());

            }
        }
        return Arrays.asList(avatarSkillExcelConfigData);
    }


    private static Map<String, String> textMapRU(String filename) throws IOException {
        System.out.println("\nfilenames:" + filename);
        //List<String> arrayList = new ArrayList<>();
        Map<String, String> mapTextMapRU = mapper.readValue(Paths.get(filename).toFile(), Map.class);
        // for (Map.Entry<String, String> entry : mapTextMapRU.entrySet()) {

        //if (!arrayList.contains(entry.getKey())) {
        //System.out.println("@JsonProperty(\""+k+"\") private"+v.getClass()+" "+k+";");
        //arrayList.add(entry.getKey());
        //  }

        //  }
        //System.out.println("\narrayList:" + arrayList.size());

        return mapTextMapRU;
    }

}
