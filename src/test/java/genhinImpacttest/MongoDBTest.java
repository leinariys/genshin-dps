package genhinImpacttest;

import com.mongodb.*;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;
import org.bson.BsonArray;
import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.Document;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class MongoDBTest {


    private static List<Path> listFile = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        fileRead();
        //  mongoDbOld();
        mongoDbNew();
    }


    private static void fileRead() throws IOException {
        File fileDate = new File("C:\\Users\\leina\\Downloads\\GenshinData-master\\TextMap");
        Files.find(Paths.get(String.valueOf(fileDate)),
                Integer.MAX_VALUE,
                (filePath, fileAttr) -> fileAttr.isRegularFile() && filePath.toString().indexOf("InterAction") == -1)
                .forEach((filePath) -> {
                            if (filePath.toString().endsWith("json")) {
                                listFile.add(filePath);
                            }
                        }
                );
    }


    private static void mongoDbOld() throws IOException {

        MongoURI mongoURI = new MongoURI("mongodb://mongoadminAr:secreter@192.168.1.9:27017");
        Mongo mongo = new Mongo(mongoURI);
        DB db = mongo.getDB("GenshinData");

        for (Path fileJson : listFile) {

            String collect = "";
            for (int i = 4; i < fileJson.getNameCount() - 1; i++) {
                collect += "_" + fileJson.getName(i);
            }
            collect = collect.substring(1, collect.length());

            DBCollection collection;
            if (db.collectionExists(collect)) {
                collection = db.getCollection(collect);
            } else {
                collection = db.createCollection(collect, new BasicDBObject());
            }


            DBObject dbObject = (DBObject) JSON.parse(Files.readString(fileJson, StandardCharsets.US_ASCII));
            collection.insert(dbObject);
        }

    }

    private static void mongoDbNew() {

        //ConnectionString connString = new ConnectionString("mongodb://mongoadminAr:secreter@192.168.1.9:27017");
        ConnectionString connString = new ConnectionString("mongodb+srv://mongoadminAr:secreter@cluster0.srtlw.mongodb.net/test?authSource=admin&replicaSet=atlas-b6884v-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true");
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(connString)
                .retryWrites(true)
                .build();

        try (MongoClient mongoClient = MongoClients.create(settings)) {
            MongoDatabase database = mongoClient.getDatabase("GenshinData");

            for (Path fileJson : listFile) {

                String collect = "";
                for (int i = 4; i < fileJson.getNameCount() - 1; i++) {
                    collect += "_" + fileJson.getName(i);
                }
                collect = collect.substring(1, collect.length());

                if (database.getCollection(collect) == null) {
                    database.createCollection(collect);
                }
                MongoCollection<Document> collection = database.getCollection(collect);

                String readString = Files
                        .readString(fileJson, StandardCharsets.UTF_8)
                        .replace('$', '_')
                        .replaceAll("[a-zA-Z]\\.", "_");


                try {
                    Document nameFile;
                    if (readString.charAt(0) == '[') {
                        BsonArray document = BsonArray.parse(readString);
                        nameFile = new Document()
                                .append("file_name", new BsonString(fileJson.getName(fileJson.getNameCount() - 1).toString()))
                                .append("file_document", document);
                    } else {
                        BsonDocument document = BsonDocument.parse(readString);
                        nameFile = new Document()
                                .append("file_name", new BsonString(fileJson.getName(fileJson.getNameCount() - 1).toString()))
                                .append("file_document", document);
                    }

                    collection.insertOne(nameFile);

                } catch (Exception e) {
                    System.out.println("Exception:" + e);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}