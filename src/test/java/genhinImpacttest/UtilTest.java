package genhinImpacttest;

import genhin.Util;
import genhin.data.DbEnemie;
import genhin.json.*;
import genhin.math.AttackValue;
import genhin.math.StatisticCrit;
import genhin.math.WorkState;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


public class UtilTest {

    public Hero newHero(String name, float val, boolean isAffectsGroup) {
        Hero newChar = new Hero();
        newChar.setName(name);
        newChar.setValueOption(
                new ValueOption(TypeGroup.WEAPON, TypeOption.ELM, val, isAffectsGroup),
                new ValueOption(TypeGroup.WEAPON, TypeOption.ATK, val, isAffectsGroup),
                new ValueOption(TypeGroup.WEAPON, TypeOption.ATK_PERCENT, val, isAffectsGroup),
                new ValueOption(TypeGroup.WEAPON, TypeOption.CRIT_CHANCE_PERCENT, val, isAffectsGroup),
                new ValueOption(TypeGroup.WEAPON, TypeOption.CRIT_PERCENT, val, isAffectsGroup),
                new ValueOption(TypeGroup.WEAPON, TypeOption.FIRE_PERCENT, val, isAffectsGroup),
                //  new ValueOption(TypeGroup.WEAPON, TypeOption.RES_PERCENT, -val, TypeElement.ANEMO),
                new ValueOption(TypeGroup.CHARACTER, TypeOption.ELM, val, isAffectsGroup),
                new ValueOption(TypeGroup.CHARACTER, TypeOption.ATK, val, isAffectsGroup),
                new ValueOption(TypeGroup.CHARACTER, TypeOption.DEF, val, isAffectsGroup),
                new ValueOption(TypeGroup.CHARACTER, TypeOption.CRIT_PERCENT, val, isAffectsGroup),
                new ValueOption(TypeGroup.ARTIFACT, TypeOption.ELM, val, isAffectsGroup),
                new ValueOption(TypeGroup.ARTIFACT, TypeOption.ATK, val, isAffectsGroup),
                new ValueOption(TypeGroup.ARTIFACT, TypeOption.ATK_PERCENT, val, isAffectsGroup),
                new ValueOption(TypeGroup.ARTIFACT, TypeOption.ANEMO_PERCENT, val, isAffectsGroup),
                new ValueOption(TypeGroup.ARTIFACT, TypeOption.ATK_UNITS, val, isAffectsGroup)
        );
        return newChar;
    }

    private GroupHero testGroup() {
        GroupHero groupHero = new GroupHero();
        Hero selectNew = newHero("Hero1", 10.0f, false);
        selectNew.setSelect(true);

        groupHero.setCharact(selectNew);
        groupHero.setCharact(newHero("Hero2", 20.0f, true));
        groupHero.setCharact(newHero("Hero3", 30.0f, false));
        groupHero.setCharact(newHero("Hero4", 40.0f, false));
        return groupHero;
    }

    private Enemies testEnemies() {
        Enemies enemie = new Enemies(DbEnemie
                .hilichurl()
                .get(0)
                .getName(), 50);
        enemie.setOptions(DbEnemie.hilichurl());

        return enemie;
    }


    @Test
    public void selectHero() {

        GroupHero groupHero = testGroup();

        Hero select = Util.getSelectHero(groupHero);
        List< Hero> heroMap = Util.getOtherHero(groupHero);
        System.out.println(select.toString());
       // heroMap.stream().forEach(System.out::println);

        assertNotEquals(select, null);
        assertEquals(heroMap.size(), 3);
    }

    @Test
    public void allOptionsSize() {
        GroupHero groupHero = testGroup();

        List<ValueOption> allOptions = Util.getSelectHeroValueOption(groupHero);
        assertEquals(allOptions.size(), 8);

        allOptions = Util.getHerosAffectsGroup(groupHero);
        assertEquals(allOptions.size(), 8);

    }


    @Test
    public void calcValueTypeOption() {
        GroupHero groupHero = testGroup();
        Double atk = Util.calcValueTypeOption(groupHero, TypeOption.ATK, TypeOption.ATK_PERCENT, TypeOption.ATK_UNITS);
        assertEquals(atk, 126.0d);
    }


    @Test
    public void coefficientEm() {
        GroupHero groupHero = testGroup();

        Double elm = Util.coefficientEm(groupHero);
        Double elm1 = Util.coefficientEm1(elm);
        Double elm2 = Util.coefficientEm2(elm);
        Double elm3 = Util.coefficientEm3(elm);

        assertEquals(elm, 90.0d);
        assertEquals(elm1, 16.8d);
        assertEquals(elm2, 40.3d);
        assertEquals(elm3, 26.9d);
    }

    @Test
    public void calcElementCrit() {
        GroupHero groupHero = testGroup();

        Double critChance = Util.coefficientCritChance(groupHero);
        Double critDmg = Util.coefficientCritDmg(groupHero);
        Double firePercent = Util.calcElement(groupHero, TypeOption.FIRE_PERCENT);

        assertEquals(critChance, 30.0d);
        assertEquals(critDmg, 60.0d);
        assertEquals(firePercent, 30.0d);

    }


    ////////////////////////////////////////////////////
    @Test
    public void coefficientDmgBonus() {
        GroupHero groupHero = testGroup();

        Double critChance = Util.coefficientDmgBonus(groupHero, TypeOption.ANEMO_PERCENT);

        assertEquals(critChance, 1.3d);
    }

    @Test
    public void calcResTest() {
        double res = -20.0d;
        double resEnemies = 10.d;
        res = Util.calcRes(res, resEnemies);
        assertEquals(res, 1.05d);

        res = 20.0d;
        resEnemies = 10.d;
        res = Util.calcRes(res, resEnemies);
        assertEquals(res, 0.7d);

        res = 70.0d;
        resEnemies = 10.d;
        res = Util.calcRes(res, resEnemies);
        assertEquals(res, 0.23809523809523814d);


        GroupHero groupHero = testGroup();
        Enemies enemies = testEnemies();
        Double coefficientRes = Util.coefficientRes(groupHero, enemies, TypeElement.ANEMO);
        assertEquals(coefficientRes, 1.05d);
    }

    @Test
    public void coefficientDef() {
        GroupHero groupHero = testGroup();
        Enemies enemies = testEnemies();
        Double coefficientDef = Util.coefficientDef(groupHero, enemies);

        assertEquals(coefficientDef, 1.3d);
    }

    @Test
    public void coefficientCrit() {
        GroupHero groupHero = testGroup();
        Enemies enemies = testEnemies();
        WorkState workState = new WorkState(groupHero, enemies);
        StatisticCrit coefficientCrit = Util.coefficientCrit(workState.getCharacteristic());

        assertEquals(coefficientCrit.getCritDmgAverage(), 41.9088d);
    }

    @Test
    public void coefficientTalentAttack() {
        GroupHero groupHero = testGroup();
        Enemies enemies = testEnemies();
        WorkState workState = new WorkState(groupHero, enemies);

        List<AttackValue> attackValues = Util.valueTalentAttack(workState.getCharacteristic(), groupHero, 1, TypeAbility.ATTACK);

        assertEquals(attackValues.size(), 1.3d);
    }


}
