package genhinImpacttest;

import genhin.Util;
import genhin.data.DbEnemie;
import genhin.json.*;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReadWriteTest {

    static final String author = "set   Author";
    static final String version = "set   Version";
    static final String contactForCommunication = "set Contact For Communication";
    static final String setName = "Имя перса";
    static final String pathname = "src/test/resources/char-list-test.json";

    @Test
    public void write() {
        File fileCharList = new File(pathname);
        DpsMetrConteiner dpsMetrConteiner = new DpsMetrConteiner();

        dpsMetrConteiner.setVersion(version);
        dpsMetrConteiner.setAuthor(author);
        dpsMetrConteiner.setContact(contactForCommunication);

        GroupHero groupCharact = new GroupHero();
        groupCharact.setCharact(newCharacter(), newCharacter(), newCharacter());
        dpsMetrConteiner.setGroupCharacter(groupCharact, groupCharact);

        dpsMetrConteiner.setEnemies(DbEnemie.getEnemies());

        Util.write(dpsMetrConteiner, fileCharList);
    }

    private Hero newCharacter() {
        Hero hero = new Hero();
        hero.setName(setName);
        hero.setValueOption(
                new ValueOption(TypeGroup.CHARACTER, TypeOption.HP, 10_194.0f),
                new ValueOption(TypeGroup.TALENT, TypeOption.ATK_PERCENT, 44.4f),
                new ValueOption(TypeGroup.TALENT, TypeOption.ATK_PERCENT, 2)
        );
        return hero;
    }


    @Test
    public void read() {
        File fileCharList = new File(pathname);
        DpsMetrConteiner dpsMetrConteiner;
        dpsMetrConteiner = Util.reader(fileCharList);

        assertEquals(dpsMetrConteiner.getAuthor(), author);
        assertEquals(dpsMetrConteiner.getVersion(), version);

        System.out.println("println:" + author + " : " + version);

    }

}
